# README 

This is the Bitbucket site for the HS2015 Software Engineering Project of Englishgroup 3.
Information about the project will be documented in the Movie Database Project site. 

## News

Fr. 20.11.2015:

New source folder for the unit tests. Please add the folder test as source folder to your eclipse project. 

`right click the folder / Build Path / Use as Source Folder`


### Wiki & Projectdocumentation

* [Movie Database Project](https://bitbucket.org/hs15_se_english3/hs15-software-engineering/wiki/Movie%20Database%20Project)
* [Web Interface](https://bitbucket.org/hs15_se_english3/hs15-software-engineering/wiki/WebApp%20Interface)
* [Worksheet](https://bitbucket.org/hs15_se_english3/hs15-software-engineering/wiki/Work)
* [Sprints and Backlogs](https://bitbucket.org/hs15_se_english3/hs15-software-engineering/wiki/Sprints%20and%20Backlogs)

### Working with git

Get latest state of remote repository for the active branch

`git pull <origin> <branchname>`

Stage and commit

```
git status			# to list the local changes
git add	<filename>	# to add a changed file to stageing area
git commit 			# to commit the stageing area
git push			# to push the commit to the remote repository
```

Always commit with a useful comment!


Create working branch

```
git branch <branchname>		# to create new branch
git checkout <branchname>	# to switch to the new working branch

-- shortcut
git checkout -b <branchname>	# same as above
```

Merge working branch to master and synch with remote repo

```
git checkout master
git pull               		# to update the state to the latest remote master state
git merge <working branch>  # to bring changes to local master from your develop branch
git push origin master 		# push current HEAD to remote master branch
```

### Helpful links ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Git tutorial (de)](https://rogerdudler.github.io/git-guide/index.de.html)
* [Git Cheatsheet (de)](https://rogerdudler.github.io/git-guide/files/git_cheat_sheet.pdf)
* [Git simple branching](https://git-scm.com/book/de/v1/Git-Branching-Einfaches-Branching-und-Merging)

### Contribution guidelines ###

* We write tests
* We commit only after the test have run successfully 
* We use feature branches (if we start a new task, we create a new feature branche, so we can try and do what ever we want without interfering with the others work)
* We use pull requests before merging feature branches (that way at least someone else sees our code. Four eyes see more than two and we share knowledge about the codebase)