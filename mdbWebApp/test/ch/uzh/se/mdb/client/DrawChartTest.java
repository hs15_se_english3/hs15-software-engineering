package ch.uzh.se.mdb.client;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.uzh.se.mdb.server.DataLibrary;
import ch.uzh.se.mdb.shared.FilmCountData;
import ch.uzh.se.mdb.shared.FilmFilter;

/**
 * Tests functionality of the drawChart method.
 * @author Sarah, Mervin
**/
public class DrawChartTest {
	
	DataLibrary library = new DataLibrary();
	FilmFilter filter = new FilmFilter();

	//FilmFilter filter = null;
	private String country_1, country_2, country_3;
	private String genre_1, genre_2, genre_3; 
	private int expected_1, expected_2, expected_3;
	
	@Before
	public void setUp() {		
		/* Test case 1: count the World Cinema films in Switzerland*/
		country_1 = "Switzerland";
		genre_1 = "World Cinema";
		expected_1 = 89;

		/* Test case 2: count the Romance films in England*/
		country_2 = "England";
		genre_2 = "Romance Film";
		expected_2 = 50;
		
		/* Test case 3: count the Anime films in Japan*/
		country_3 = "Japan";
		genre_3 = "Anime";
		expected_3 = 356;

	}
	
	@Test
	public void testGenreCount1() {	
		//filter.setCountryFilter(country_1);
		filter.setGenreFilter(genre_1);
		List<FilmCountData> result_genre1 = library.countFilmsGroupBy(filter,"Countries");
		int count1 = 0;
		for(int i = 0; i < result_genre1.size(); i++) {
			String result = result_genre1.get(i).getValue();
			if(result.equals(country_1)) {
				count1 = result_genre1.get(i).getCount();
			}
		}
		Assert.assertEquals(expected_1, count1 );
	}
	
	@Test
	public void testGenreCount2() {	
		//filter.setCountryFilter(country_1);
		filter.setGenreFilter(genre_2);
		List<FilmCountData> result_genre2 = library.countFilmsGroupBy(filter,"Countries");
		int count2 = 0;
		for(int i = 0; i < result_genre2.size(); i++) {
			String result = result_genre2.get(i).getValue();
			if(result.equals(country_2)) {
				count2 = result_genre2.get(i).getCount();
			}
		}
		Assert.assertEquals(expected_2, count2 );
	}
	
	@Test
	public void testGenreCount3() {
		//filter.setCountryFilter(country_1);
		filter.setGenreFilter(genre_3);
		List<FilmCountData> result_genre3 = library.countFilmsGroupBy(filter,"Countries");
		int count3 = 0;
		for(int i = 0; i < result_genre3.size(); i++) {
			String result = result_genre3.get(i).getValue();
			if(result.equals(country_3)) {
				count3 = result_genre3.get(i).getCount();
			}
		}
		Assert.assertEquals(expected_3, count3 );
	}
}

