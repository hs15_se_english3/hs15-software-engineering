package ch.uzh.se.mdb.server;

import java.util.*;

import ch.uzh.se.mdb.shared.*;

import junit.framework.TestCase;

/**
 * Tests functionality of the <c>DataLibrary</c> class.
 * @author Ada
 * 
 * Note: No need to test for exceptions - in jUnit, an exception *is* a failure,
 * see http://stackoverflow.com/questions/17731234.
 */
public class DataLibraryTest extends TestCase {
	DataLibrary library = new DataLibrary();

	public void testFilterFilms() {
		FilmFilter filter = new FilmFilter(
				"ghost", null, null, 10, 100, null, null, "english", "united states", "horror");
		
		List<FilmData> result = library.filterFilms(filter);
		
		// Check result count.
		assertNotNull(result);
		assertEquals(15, result.size());
	}
	
	public void testFilterFilmsLimitAndPaging() {
		FilmFilter filter = new FilmFilter();
		
		// Get default number of films without paging.
		List<FilmData> result = library.filterFilms(filter);
		
		// Get 10 films offset by 100.
		List<FilmData> resultOffset = library.filterFilms(filter, new FilmPaging(100, 10)).getResultList();

		// Check default and explicit limit.
		assertEquals(1000, result.size());
		assertEquals(10, resultOffset.size());

		// Check offset.
		assertEquals(result.get(100).getTextId(), resultOffset.get(0).getTextId());
		assertEquals(result.get(109).getTextId(), resultOffset.get(9).getTextId());
	}

	public void testCountFilms() {
		FilmFilter filter = new FilmFilter(
				"ghost", null, null, 10, 100, null, null, "english", "united states", "horror");
		
		int result = library.countFilms(filter);
		
		// Check result count.
		assertEquals(15, result);
	}

	public void testCountFilmsGroupByCountry() {
		FilmFilter filter = new FilmFilter(
				"ghost", null, null, 10, 100, null, null, null, null, null);
		
		List<FilmCountData> result = library.countFilmsGroupBy(filter, "Countries");
		
		// Check result count.
		assertNotNull(result);
		assertEquals(19, result.size());
		
		// Check individual counts.
		assertEquals(51, getByValue(result, "United States of America").getCount());
		assertEquals(13, getByValue(result, "Hong Kong").getCount());
		assertNull(getByValue(result, "Horror"));
	}

	public void testCountFilmsGroupByGenre() {
		FilmFilter filter = new FilmFilter(
				"ghost", null, null, 10, 100, null, null, null, null, null);
		
		List<FilmCountData> result = library.countFilmsGroupBy(filter, "Genres");
		
		// Check result count.
		assertNotNull(result);
		assertEquals(77, result.size());
		
		// Check individual counts.
		assertEquals(46, getByValue(result, "Horror").getCount());
		assertEquals(26, getByValue(result, "Comedy").getCount());
		assertNull(getByValue(result, "Hong Kong"));
	}
	
	/**
	 * Returns the item in the list with the given value, null if not found.
	 */
	private FilmCountData getByValue(List<FilmCountData> list, String value) {
		for (FilmCountData item : list) {
			if (item.getValue().equals(value)) {
				return item;
			}
		}
		
		return null;
	}
}
