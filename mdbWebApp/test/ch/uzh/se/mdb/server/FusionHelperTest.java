package ch.uzh.se.mdb.server;

import junit.framework.TestCase;

/**
 * Tests functionality of the <c>FusionHelper</c> class.
 * @author Ada
 * 
 * Note: No need to test for exceptions - in jUnit, an exception *is* a failure,
 * see http://stackoverflow.com/questions/17731234.
 */
public class FusionHelperTest extends TestCase {

	public void testParsePairs() {
		String json1 = "{\"/m/01jfsb\": \"Thriller\", \"/m/06n90\": \"Science Fiction\"}";
		String json2 = "{\"/m/02h40lc\": \"English Language\"}";

		FusionHelper.parsePairs(json1);
		FusionHelper.parsePairs(json2);
	}

	public void testParseFilms() {
		String json = 
			  "{"
			+ " \"kind\": \"fusiontables#sqlresponse\","
			+ " \"columns\": ["
			+ "  \"ID\","
			+ "  \"TextID\","
			+ "  \"Title\","
			+ "  \"ReleaseDate\","
			+ "  \"Earnings\","
			+ "  \"Duration\","
			+ "  \"Languages\","
			+ "  \"Countries\","
			+ "  \"Genres\""
			+ ""
			+ " ],"
			+ " \"rows\": ["
			+ "  ["
			+ "   \"975900\","
			+ "   \"/m/03vyhn\","
			+ "   \"Ghosts of Mars\","
			+ "   \"24.08.2001\","
			+ "   \"14010832\","
			+ "   \"98\","
			+ "   \"{\\\"/m/02h40lc\\\": \\\"English Language\\\"}\","
			+ "   \"{\\\"/m/09c7w0\\\": \\\"United States of America\\\"}\","
			+ "   \"{\\\"/m/01jfsb\\\": \\\"Thriller\\\", \\\"/m/06n90\\\": \\\"Science Fiction\\\", \\\"/m/03npn\\\": \\\"Horror\\\", \\\"/m/03k9fj\\\": \\\"Adventure\\\", \\\"/m/0fdjb\\\": \\\"Supernatural\\\", "
			+ ""
			+ "\\\"/m/02kdv5l\\\": \\\"Action\\\", \\\"/m/09zvmj\\\": \\\"Space western\\\"}\""
			+ "  ],"
			+ "  ["
			+ "   \"28463795\","
			+ "   \"/m/0crgdbh\","
			+ "   \"Brun bitter\","
			+ "   \"1988\","
			+ "   \"NaN\","
			+ "   \"83\","
			+ "   \"{\\\"/m/05f_3\\\": \\\"Norwegian Language\\\"}\","
			+ "   \"{\\\"/m/05b4w\\\": \\\"Norway\\\"}\","
			+ "   \"{\\\"/m/0lsxr\\\": \\\"Crime Fiction\\\", \\\"/m/07s9rl0\\\": \\\"Drama\\\"}\""
			+ "  ],"
			+ "  ["
			+ "   \"9363483\","
			+ "   \"/m/0285_cd\","
			+ "   \"White Of The Eye\","
			+ "   \"1987\","
			+ "   \"NaN\","
			+ "   \"110\","
			+ "   \"{\\\"/m/02h40lc\\\": \\\"English Language\\\"}\","
			+ "   \"{\\\"/m/07ssc\\\": \\\"United Kingdom\\\"}\","
			+ "   \"{\\\"/m/01jfsb\\\": \\\"Thriller\\\", \\\"/m/0glj9q\\\": \\\"Erotic thriller\\\", \\\"/m/09blyk\\\": \\\"Psychological thriller\\\"}\""
			+ "  ]"
			+ " ]"
			+ "}";

		// Check that result is parsed correctly.
		FusionHelper.parseFilms(json);
	}

	public void testParseCount() {
		String json = 
			  "{"
			+ " \"kind\": \"fusiontables#sqlresponse\","
			+ " \"columns\": ["
			+ "  \"count()\""
			+ " ],"
			+ " \"rows\": ["
			+ "  ["
			+ "   \"15\""
			+ "  ]"
			+ " ]"
			+ "}";

		// Check that result is parsed correctly.
		FusionHelper.parseCount(json);
	}

	public void testParseCountsGroupBy() {
		String json = 
			  "{"
			+ " \"kind\": \"fusiontables#sqlresponse\","
			+ " \"columns\": ["
			+ "  \"Genres\","
			+ "  \"count()\""
			+ " ],"
			+ " \"rows\": ["
			+ "  ["
			+ "   \"{\\\"/m/01jfsb\\\": \\\"Thriller\\\", \\\"/m/03npn\\\": \\\"Horror\\\", \\\"/m/0fdjb\\\": \\\"Supernatural\\\"}\","
			+ "   \"1\""
			+ "  ],"
			+ "  ["
			+ "   \"{\\\"/m/02hmvc\\\": \\\"Short Film\\\"}\","
			+ "   \"1\""
			+ "  ]"
			+ " ]"
			+ "}";

		// Check that result is parsed correctly.
		FusionHelper.parseCountsGroupBy(json);
	}
}
