package ch.uzh.se.mdb.server;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;


/**
 * Tests functionality of the <c>HttpHelper</c> class.
 * @author Ada
 * 
 * Note: No need to test for exceptions - in jUnit, an exception *is* a failure,
 * see http://stackoverflow.com/questions/17731234.
 */
public class HttpHelperTest {

	@Test
	public void testGetGoogle() {
		String url = "https://www.google.com";
		String result = HttpHelper.get(url);
		Assert.assertTrue("Contains google logo", result.contains("/images/branding/googlelogo"));
	}
	
	@Test
	public void testGetFusionString() throws URISyntaxException {
		String url = "https://www.googleapis.com/fusiontables/v2/query?key=AIzaSyBIXKz3WcHBqRWUPNyzzSK7SWF7kduhWZo&sql=SELECT%20*%20FROM%201m099QSF8XRmXkM1ESLRxj9kC3LTCimOv9lq4VGtB%20LIMIT%201";
		String result = HttpHelper.get(url);
		Assert.assertTrue("Is fustion table response", result.contains("fusiontables#sqlresponse"));
	}
	
	@Test
	public void testGetFusionUri() throws URISyntaxException {
		URI url = new URI("https", "www.googleapis.com", "/fusiontables/v2/query", "key=AIzaSyBIXKz3WcHBqRWUPNyzzSK7SWF7kduhWZo&sql=SELECT * FROM 1m099QSF8XRmXkM1ESLRxj9kC3LTCimOv9lq4VGtB LIMIT 1", null);
		String result = HttpHelper.get(url);
		Assert.assertTrue("Is fustion table response", result.contains("fusiontables#sqlresponse"));
	}
}
