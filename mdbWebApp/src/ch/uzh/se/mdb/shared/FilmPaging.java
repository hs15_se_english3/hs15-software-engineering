package ch.uzh.se.mdb.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Represents user-provided paging conditions.
 * @author Ada
 */
public class FilmPaging implements IsSerializable {
	
	public static final Integer DEFAULT_LIMIT = 1000; 

	private Integer offset;
	private Integer limit;
	
	private static FilmPaging def = new FilmPaging(null, 1000);
	
	public FilmPaging() {
		// Default constructor needed for serialization.
		limit = DEFAULT_LIMIT;
	}

	public FilmPaging(Integer offset, Integer limit) {
		this.setOffset(offset);
		this.setLimit(limit);
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

}
