package ch.uzh.se.mdb.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Represents user-provided filter conditions.
 * @author Ada
 */
public class FilmFilter implements IsSerializable {

	private String titleFilter;
	private Integer releaseDateFromFilter;
	private Integer releaseDateToFilter;
	private Integer durationFromFilter;
	private Integer durationToFilter;
	private Integer earningsFromFilter;
	private Integer earningsToFilter;
	private String languageFilter;
	private String countryFilter;
	private String genreFilter;

	public FilmFilter() {
		// Default constructor needed for serialization.
	}

	public FilmFilter(String titleFilter, Integer releaseDateFromFilter, Integer releaseDateToFilter, 
			Integer durationFromFilter, Integer durationToFilter, Integer earningsFromFilter, Integer earningsToFilter,
			String languageFilter, String countryFilter, String genreFilter) {
		this.titleFilter = titleFilter;
		this.releaseDateFromFilter = releaseDateFromFilter;
		this.releaseDateToFilter = releaseDateFromFilter;
		this.durationFromFilter = durationFromFilter;
		this.durationToFilter = durationToFilter;
		this.earningsFromFilter = earningsFromFilter;
		this.earningsToFilter = earningsToFilter;
		this.languageFilter = languageFilter;
		this.countryFilter = countryFilter;
		this.genreFilter = genreFilter;
	}

	public String getTitleFilter() {
		return titleFilter;
	}

	public void setTitleFilter(String titleFilter) {
		this.titleFilter = titleFilter;
	}
	public Integer getReleaseDateFromFilter() {
		return releaseDateFromFilter;
	}

	public void setReleaseDateFromFilter(Integer releaseDateFromFilter) {
		this.releaseDateFromFilter = releaseDateFromFilter;
	}

	public Integer getReleaseDateToFilter() {
		return releaseDateToFilter;
	}

	public void setReleaseDateToFilter(Integer releaseDateToFilter) {
		this.releaseDateToFilter = releaseDateToFilter;
	}
	
	public Integer getDurationFromFilter() {
		return durationFromFilter;
	}
	
	public void setDurationFromFilter(Integer durationFilter) {
		this.durationFromFilter = durationFilter;
	}
	
	public Integer getDurationToFilter() {
		return durationToFilter;
	}

	public void setDurationToFilter(Integer durationToFilter) {
		this.durationToFilter = durationToFilter;
	}

	public Integer getEarningsFromFilter() {
		return earningsFromFilter;
	}

	public void setEarningsFromFilter(Integer earningsFromFilter) {
		this.earningsFromFilter = earningsFromFilter;
	}

	public Integer getEarningsToFilter() {
		return earningsToFilter;
	}

	public void setEarningsToFilter(Integer earningsToFilter) {
		this.earningsToFilter = earningsToFilter;
	}

	public String getLanguageFilter() {
		return languageFilter;
	}
	
	public void setLanguageFilter(String languageFilter) {
		this.languageFilter = languageFilter;
	}

	public String getCountryFilter() {
		return countryFilter;
	}
	
	public void setCountryFilter(String countryFilter) {
		this.countryFilter = countryFilter;
	}
	
	public String getGenreFilter() {
		return genreFilter;
	}
	
	public void setGenreFilter(String genreFilter) {
		this.genreFilter = genreFilter;
	}

}
