package ch.uzh.se.mdb.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Contains information about a specific language, genre or country 
 * and the count of matching films.
 * @author Ada
 */
public class FilmCountData implements IsSerializable {
	
	private String key;
	private String value;
	private int count;
	
	public FilmCountData() {
		// Default constructor needed for serialization.
	}
	
	public FilmCountData(String key, String value) {
		this.key = key;
		this.value = value;
		this.count = 0;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
