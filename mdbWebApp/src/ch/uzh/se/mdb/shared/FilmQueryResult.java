package ch.uzh.se.mdb.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class FilmQueryResult implements IsSerializable {

	private int queryResultSize;
	private List<FilmData> resultList;
	
	public FilmQueryResult() {}
	
	public FilmQueryResult(int resultSize, List<FilmData> list) {
		this.queryResultSize = resultSize;
		this.resultList = list;
	}

	public int getQueryResultSize() {
		return queryResultSize;
	}

	public List<FilmData> getResultList() {
		return resultList;
	}
	
	
	
}
