package ch.uzh.se.mdb.shared;

import java.util.*;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Contains information about a movie as stored in the data store.
 * @author Ada
 */
public class FilmData implements IsSerializable {

	// TODO: Handle missing/nullable data.
	private int id;
	private String textId;
	private String title;
	private Date releaseDate;
	private int earnings;
	private int duration;
	private Map<String, String> languages;
	private Map<String, String> countries;
	private Map<String, String> genres;

	public FilmData() {
		// Default constructor needed for serialization.
	}

	public FilmData(int id, String textId, String title, Date releaseDate, int earnings, int duration) {
		this(id, textId, title, releaseDate, earnings, duration, new HashMap<String, String>(), new HashMap<String, String>(), new HashMap<String, String>());
	}

	public FilmData(int id, String textId, String title, Date releaseDate, int earnings, int duration,
			Map<String, String> languages, Map<String, String> countries, Map<String, String> genres) {
		super();
		this.id = id;
		this.textId = textId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.earnings = earnings;
		this.duration = duration;
		this.languages = languages;
		this.countries = countries;
		this.genres = genres;
	}

	/**
	 * Gets the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the textId
	 */
	public String getTextId() {
		return textId;
	}

	/**
	 * @param textId the textId to set
	 */
	public void setTextId(String textId) {
		this.textId = textId;
	}

	/**
	 * @return the releaseDate
	 */
	public Date getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @param releaseDate the releaseDate to set
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the earnings
	 */
	public int getEarnings() {
		return earnings;
	}

	/**
	 * @param earnings the earnings to set
	 */
	public void setEarnings(int earnings) {
		this.earnings = earnings;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the languages
	 */
	public Map<String, String> getLanguages() {
		return languages;
	}

	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(Map<String, String> languages) {
		this.languages = languages;
	}

	/**
	 * @return the countries
	 */
	public Map<String, String> getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public void setCountries(Map<String, String> countries) {
		this.countries = countries;
	}

	/**
	 * @return the genres
	 */
	public Map<String, String> getGenres() {
		return genres;
	}

	/**
	 * @param genres the genres to set
	 */
	public void setGenres(Map<String, String> genres) {
		this.genres = genres;
	}
}
