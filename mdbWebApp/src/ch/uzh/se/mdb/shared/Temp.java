package ch.uzh.se.mdb.shared;

import java.io.Serializable;
import java.util.Date;

public class Temp implements Serializable {

	private String country;
	private Date start;
	private Date end;
	private int count = 0;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
