package ch.uzh.se.mdb.server;

import ch.uzh.se.mdb.client.*;
import ch.uzh.se.mdb.shared.*;

import java.util.*;

import com.google.gwt.user.server.rpc.*;

/**
 * The server-side implementation of the RPC service.
 * @author Ada
 */
@SuppressWarnings("serial")
public class FilmServiceImpl extends RemoteServiceServlet implements
    FilmService {
	
	private final DataLibrary library = new DataLibrary();
	
	/**
	 * Tests the connection between client and server.
	 */
	public String test() {
		return "Service test successful";
	}
	
	/**
	 * Returns the list of films matching the given filter.
	 */
	public List<FilmData> filter(FilmFilter filter) throws IllegalArgumentException {
		return library.filterFilms(filter);
	}
	
	/**
	 * Return the list of films matching the given filter and paging.
	 */
	public FilmQueryResult filter(FilmFilter filter, FilmPaging paging) throws IllegalArgumentException {
		return library.filterFilms(filter, paging);
	}
	
	/**
	 * Returns the count of films matching the given filter.
	 */
	public Integer count(FilmFilter filter) throws IllegalArgumentException {
		return library.countFilms(filter);
	}
	
	/**
	 * Returns a list of counts of films matching the given filter
	 * grouped by the given field.
	 */
	public List<FilmCountData> countGroupBy(FilmFilter filter, String groupByField) throws IllegalArgumentException {
		return library.countFilmsGroupBy(filter, groupByField);
	}
}


