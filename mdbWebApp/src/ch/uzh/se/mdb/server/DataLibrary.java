package ch.uzh.se.mdb.server;

import java.net.*;
import java.util.*;

import ch.uzh.se.mdb.shared.*;

import com.google.gson.JsonSyntaxException;

/**
 * Responsible for data storage, querying and modifying data.
 * Integrates <c>FilterEngine</c> functionality.
 * @author Ada
 */
public class DataLibrary {

	// Base URI fragements for SELECT queries against Fusion Tables.
	private static final String uriScheme = "https";
	private static final String uriHost = "www.googleapis.com";
	private static final String uriPath = "/fusiontables/v2/query";
	private static final String uriQueryBaseSelect = "key=AIzaSyBIXKz3WcHBqRWUPNyzzSK7SWF7kduhWZo&sql=SELECT * FROM 1m099QSF8XRmXkM1ESLRxj9kC3LTCimOv9lq4VGtB %s";
	private static final String uriQueryBaseCount = "key=AIzaSyBIXKz3WcHBqRWUPNyzzSK7SWF7kduhWZo&sql=SELECT COUNT() FROM 1m099QSF8XRmXkM1ESLRxj9kC3LTCimOv9lq4VGtB %s";
	private static final String uriQueryBaseCountGroupBy = "key=AIzaSyBIXKz3WcHBqRWUPNyzzSK7SWF7kduhWZo&sql=SELECT %s, COUNT() FROM 1m099QSF8XRmXkM1ESLRxj9kC3LTCimOv9lq4VGtB %s GROUP BY %s";
	
	/**
	 * Adds a new film with the given data to the data store.
	 */
	public void addFilm(FilmData data) {
		// TODO: Convert query to REST call.
		throw new RuntimeException("Not yet implemented");
	}

	public void deleteFilm(FilmData data) {
		// TODO: Convert query to REST call.
		throw new RuntimeException("Not yet implemented");
	}

	/**
	 * Returns the list of films matching the given filter, using default 
	 * paging options.
	 */
	public List<FilmData> filterFilms(FilmFilter filter) {
		return filterFilms(filter, new FilmPaging()).getResultList();
	}
	
	/**
	 * Returns the list of films matching the given filter and paging.
	 */
	public FilmQueryResult filterFilms(FilmFilter filter, FilmPaging paging) {
		// Construct query for filter, order by and title.
		String filterQuery = getQueryFromFilter(filter);
		String orderByQuery = "ORDER BY Title ";
		String pagingQuery = getQueryFromPaging(paging);

		int resultSize = queryFilmCount(filterQuery + orderByQuery);
		List<FilmData> films = queryFilms(filterQuery + orderByQuery + pagingQuery);
		return new FilmQueryResult(resultSize, films);
	}
	
	/**
	 * Returns the count of films matching the given filter
	 * (i.e. "SELECT COUNT() WHERE filter").
	 */
	public int countFilms(FilmFilter filter) {
		// Construct query for filter.
		String query = getQueryFromFilter(filter);

		// Get count for query.
		int count = this.queryFilmCount(query);
		return count;
	}
	
	/**
	 * Returns a list of counts of films matching the given filter
	 * grouped by the given field
	 * (i.e. "SELECT groupField, COUNT() WHERE filter").
	 */
	public List<FilmCountData> countFilmsGroupBy(FilmFilter filter, String groupByField) {
		// Construct query for filter.
		String query = getQueryFromFilter(filter);

		// Get counts for query.
		List<FilmCountData> counts = this.queryFilmCountsGroupBy(query, groupByField);
		return counts;
	}
	
	/*
	 * Returns an SQL query condition for the given filter in the form
	 * "WHERE Title CONTAINS 'ghost' ".
	 */
	private String getQueryFromFilter(FilmFilter filter) {
		String query = "";
		
		// Convert filter to SQL query.
		if (filter.getTitleFilter() != null) {
			query = addCondition(query, "Title CONTAINS IGNORING CASE '" + filter.getTitleFilter().replace("'", "\\\'") + "'");
		}

		if (filter.getCountryFilter() != null) {
			query = addCondition(query, "Countries CONTAINS IGNORING CASE '" + filter.getCountryFilter().replace("'", "\\\'") + "'");
		}

		if (filter.getLanguageFilter() != null) {
			query = addCondition(query, "Languages CONTAINS IGNORING CASE '" + filter.getLanguageFilter().replace("'", "\\\'") + "'");
		}

		if (filter.getGenreFilter() != null) {
			query = addCondition(query, "Genres CONTAINS IGNORING CASE '" + filter.getGenreFilter().replace("'", "\\\'") + "'");
		}

		if (filter.getReleaseDateFromFilter() != null) {
			query = addCondition(query, "ReleaseDate >= " + filter.getReleaseDateFromFilter());
		}

		// Has to include given year itself (otherwise only first day of the entered year is included)
		if (filter.getReleaseDateToFilter() != null) {
			query = addCondition(query, "ReleaseDate < " + (filter.getReleaseDateToFilter() + 1));
		}

		if (filter.getDurationFromFilter() != null) {
			query = addCondition(query, "Duration >= " + filter.getDurationFromFilter());
		}

		if (filter.getDurationToFilter() != null) {
			query = addCondition(query, "Duration <= " + filter.getDurationToFilter());
		}

		if (filter.getEarningsFromFilter() != null) {
			query = addCondition(query, "Earnings >= " + filter.getEarningsFromFilter());
		}

		if (filter.getEarningsToFilter() != null) {
			query = addCondition(query, "Earnings <= " + filter.getEarningsToFilter());
		}

		return query;
	}
	
	private String addCondition(String query, String condition) {
		return query + (query.equals("") ? "WHERE " : "AND ") + condition + " ";
	}

	/*
	 * Returns an SQL query condition for the given paging in the form
	 * " OFFSET 100 LIMIT 200".
	 */
	private String getQueryFromPaging(FilmPaging paging) {
		String query = "";
		
		if (paging.getOffset() != null) {
			query += "OFFSET " + paging.getOffset() + " ";
		}
		
		if (paging.getLimit() != null) {
			query += "LIMIT " + paging.getLimit() + " ";
		}
		
		return query;
	}

	private List<FilmData> queryFilms(String query) {
		try {
			// Build URI (using fragment constructor so spaces in query
			// are escaped properly).
			String uriQuery = String.format(uriQueryBaseSelect, query);
			URI uri = new URI(uriScheme, uriHost, uriPath, uriQuery, null);

			// Execute REST call to Fusion Tables.
			String json = HttpHelper.get(uri);

			// Deserialize JSON response.
			List<FilmData> films = FusionHelper.parseFilms(json);

			return films;
		} catch (URISyntaxException e) {
			// Should not happen since we control the strings.
			throw new RuntimeException("Creating URL for query failed", e);
		} catch (JsonSyntaxException e) {
			// Check JSON syntax if parsing fails.
			throw new RuntimeException("Parsing JSON response failed", e);
		} catch (Exception e) {
			System.err.println(e);
			return new ArrayList<FilmData>(); 
		}
	}
	
	private int queryFilmCount(String query) {
		try {
			// Build URI (using fragment constructor so spaces in query
			// are escaped properly).
			String uriQuery = String.format(uriQueryBaseCount, query);
			URI uri = new URI(uriScheme, uriHost, uriPath, uriQuery, null);

			// Execute REST call to Fusion Tables.
			String json = HttpHelper.get(uri);

			// Deserialize JSON response.
			int count = FusionHelper.parseCount(json);

			return count;
		} catch (URISyntaxException e) {
			// Should not happen since we control the strings.
			throw new RuntimeException("Creating URL for query failed", e);
		} catch (JsonSyntaxException e) {
			// Check JSON syntax if parsing fails.
			throw new RuntimeException("Parsing JSON response failed", e);
		} catch (Exception e) {
			System.err.println(e);
			return 0; 
		}
	}

	private List<FilmCountData> queryFilmCountsGroupBy(String query, String groupByField) {
		try {
			// Build URI (using fragment constructor so spaces in query
			// are escaped properly).
			String uriQuery = String.format(uriQueryBaseCountGroupBy, groupByField, query, groupByField);
			URI uri = new URI(uriScheme, uriHost, uriPath, uriQuery, null);

			// Execute REST call to Fusion Tables.
			String json = HttpHelper.get(uri);

			// Deserialize JSON response.
			List<FilmCountData> counts = FusionHelper.parseCountsGroupBy(json);

			return counts;
		} catch (URISyntaxException e) {
			// Should not happen since we control the strings.
			throw new RuntimeException("Creating URL for query failed", e);
		} catch (JsonSyntaxException e) {
			// Check JSON syntax if parsing fails.
			throw new RuntimeException("Parsing JSON response failed", e);
		} catch (Exception e) {
			System.err.println(e);
			return new ArrayList<FilmCountData>(); 
		}
	}
}
