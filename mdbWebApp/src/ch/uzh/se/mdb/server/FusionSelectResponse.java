package ch.uzh.se.mdb.server;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Represents a de-serialized Fusion Tables JSON response.
 * @author Ada
 */
public class FusionSelectResponse extends JavaScriptObject {
	public String[][] rows;
}