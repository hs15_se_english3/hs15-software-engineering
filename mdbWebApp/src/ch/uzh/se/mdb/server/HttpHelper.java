package ch.uzh.se.mdb.server;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Provides access to response message of HTTP calls.
 * @author Ada
 * 
 * Based on descrition on StackOverflow.
 */
public class HttpHelper {
	
	private HttpHelper() {
		// Prevent instantiation.
	}

	/**
	 * Makes a GET call to the given URL and returns the content of the response
	 * interpreted as an UTF-8 string.
	 */
	public static String get(String url) {
		try {
			return get(new URI(url));
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Makes a GET call to the given URI and returns the content of the response
	 * interpreted as an UTF-8 string.
	 */
	public static String get(URI uri) {
		try {
			URL url = uri.toURL();
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();

			if (conn.getResponseCode() != 200) {
				try (InputStream s = conn.getErrorStream()) {
					String response = convertStreamToString(s);
					conn.disconnect();
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " " + response);
				}
			}

			try (InputStream s = conn.getInputStream()) {
				String response = convertStreamToString(s);
				conn.disconnect();
				return response;
			}
		}
		catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static String convertStreamToString(InputStream is) {
		try(Scanner s = new java.util.Scanner(is, "UTF-8")) {
			return s.useDelimiter("\\A").hasNext() ? s.next() : ""; 
		}
	}
}
