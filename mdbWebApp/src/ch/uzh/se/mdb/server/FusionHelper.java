package ch.uzh.se.mdb.server;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ch.uzh.se.mdb.shared.*;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Provides functionality to handle Fusion Table request and response data.
 * @author Ada
 */
public class FusionHelper {
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
	private static final SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");
	private static final Gson gson = new Gson();
	private static final Type mapType = new TypeToken<Map<String, String>>() {}.getType();		

	private FusionHelper() {
		// Prevent instantiation.
	}

	/**
	 * Deserializes a Fusion Table select query response into a list of 
	 * <c>FilmData</c>.
	 */
	public static List<FilmData> parseFilms(String json) {
		// Deserialize JSON response.
		FusionSelectResponse response = gson.fromJson(json, FusionSelectResponse.class);

		// Convert deserialized response to films.
		List<FilmData> films = new ArrayList<FilmData>();

		// Return empty list if response contains no rows.
		if (response.rows == null) {
			return films;
		}
		
		// Convert each row into a film.
		// Note: Don't use double brace initialization because
		// it creates non-serializable anonymous classes.
		for(String[] row : response.rows) {
			FilmData film = new FilmData();

			// TODO: Properly handle parsing errors, missing values.
			film.setId(FusionHelper.parseInt(row[0]));
			film.setTextId(row[1]);
			film.setTitle(row[2]);
			film.setReleaseDate(FusionHelper.parseDate(row[3]));
			film.setEarnings(FusionHelper.parseInt(row[4]));
			film.setDuration(FusionHelper.parseInt(row[5]));
			film.setLanguages(FusionHelper.parsePairs(row[6]));
			film.setCountries(FusionHelper.parsePairs(row[7]));
			film.setGenres(FusionHelper.parsePairs(row[8]));

			films.add(film);
		}
		
		// Return the rows.
		return films;
	}
	
	/**
	 * Deserializes a Fusion Table select query response into an int count.
	 */
	public static int parseCount(String json) {
		// Deserialize JSON response.
		FusionSelectResponse response = gson.fromJson(json, FusionSelectResponse.class);

		// Convert deserialized response to count.
		int count = FusionHelper.parseInt(response.rows[0][0]);

		// Return the count.
		return count;
	}

	/**
	 * Deserializes a Fusion Table select query response into a list of 
	 * <c>FilmCountData</c>.
	 */
	public static List<FilmCountData> parseCountsGroupBy(String json) {
		// Deserialize JSON response.
		FusionSelectResponse response = gson.fromJson(json, FusionSelectResponse.class);

		// Convert deserialized response to counts.
		Map<String,FilmCountData> countsMap = new Hashtable<String, FilmCountData>();

		// Return empty list if response contains no rows.
		if (response.rows == null) {
			return new ArrayList<FilmCountData>(countsMap.values());
		}
		
		// Convert each row into a count.
		// Note: Don't use double brace initialization because
		// it creates non-serializable anonymous classes.
		for(String[] row : response.rows) {
			
			// Parse response.
			Map<String, String> entries = FusionHelper.parsePairs(row[0]);
			int count = FusionHelper.parseInt(row[1]);
	
			// Because we get responses per combination of key-value-pairs,
			// loop over each pair and consolidate the counts into a dictionary
			// of unique languages, genres or countries.
			for (Entry<String, String> entry : entries.entrySet()) {
				// Get or create pair entry in dictionary.
				if (!countsMap.containsKey(entry.getKey())) {
					countsMap.put(entry.getKey(), new FilmCountData(entry.getKey(), entry.getValue()));
				}
				FilmCountData filmCount = countsMap.get(entry.getKey());

				// Increase pair count by count of response.
				filmCount.setCount(filmCount.getCount() + count);
			}
		}

		// Return the distinct list of counts.
		return new ArrayList<FilmCountData>(countsMap.values());
	}

	/**
	 * Parses an "inner JSON" string into a list of string pairs.
	 * 
	 * Examples: "{\"/m/02h40lc\": \"English Language\"}"
	 *           "{\"/m/01jfsb\": \"Thriller\", \"/m/06n90\": \"Science Fiction\"}"
	 */
	public static Map<String, String> parsePairs(String json) {
		return gson.fromJson(json, mapType);
	}

	/**
	 * Serializes a list of string pairs into "inner JSON".
	 */
	public static String serializePairs(Map<String, String> map) {
		return gson.toJson(map, mapType);
	}

	public static int parseInt(String value) {
		try {
			return value.equals("NaN") ? 0 : Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public static String serializeInt(int value) {
		return Integer.toString(value);
	}

	public static Date parseDate(String value) {
		try {
			return value.length() == 4 ? yearFormatter.parse(value) : dateFormatter.parse(value);
		} catch (Exception e) {
			return new Date(1970, 1, 1);
		}
	}

	public static String serializeDate(Date value) {
		return dateFormatter.format(value);
	}
}
