package ch.uzh.se.mdb.server;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import ch.uzh.se.mdb.shared.*;

/**
 * Provides ability to download filtered lists of films as files.
 * @author Ada
 */
@SuppressWarnings("serial")
public class DownloadService extends HttpServlet {

	private final DataLibrary library = new DataLibrary();

	/**
	 * Parses the filter in the request and returns TSV file
	 * with the films matching the filter in the response.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		// Construct filter from request URL, skipping null or empty values.
		FilmFilter filter = new FilmFilter();
		filter.setTitleFilter(getStringParameter(req, "titleFilter"));
		filter.setReleaseDateFromFilter(getIntegerParameter(req, "releaseDateFromFilter"));
		filter.setReleaseDateToFilter(getIntegerParameter(req, "releaseDateToFilter"));
		filter.setDurationFromFilter(getIntegerParameter(req, "durationFromFilter"));
		filter.setDurationToFilter(getIntegerParameter(req, "durationToFilter"));
		filter.setEarningsFromFilter(getIntegerParameter(req, "earningsFromFilter"));
		filter.setEarningsFromFilter(getIntegerParameter(req, "earningsToFilter"));
		filter.setLanguageFilter(getStringParameter(req, "languageFilter"));
		filter.setCountryFilter(getStringParameter(req, "countryFilter"));
		filter.setGenreFilter(getStringParameter(req, "genreFilter"));
		
		// Construct paging to get as many results as Fusion Tables allows (10 MByte)
		// and can be delivered in reasonable time.
		FilmPaging paging = new FilmPaging(null, 10000);
		
		// Get data matching filter.
		List<FilmData> films = library.filterFilms(filter, paging).getResultList();
		String content = serializeFilms(films);
		
		// Set header.
		resp.addHeader("content-disposition", "attachment; filename=\"export.tsv\"");
		resp.setContentType("text/tab-separated-values; charset=UTF-8");
 
		// Add content to output.
		OutputStream out = resp.getOutputStream();
		out.write(content.getBytes(Charset.forName("UTF-8")));
		out.flush();
	}
	
	private Integer getIntegerParameter(HttpServletRequest req, String parameter) {
		String value = req.getParameter(parameter);
		return (value == null || value.trim().equals("")) ? null : new Integer(value);
	}
	
	private String getStringParameter(HttpServletRequest req, String parameter) {
		String value = req.getParameter(parameter);
		return (value == null || value.trim().equals("")) ? null : value;
	}

	private String serializeFilms(List<FilmData> films) {
		StringBuilder builder = new StringBuilder();
		for (FilmData film : films) {
			builder.append(FusionHelper.serializeInt(film.getId()));
			builder.append("\t");
			builder.append(film.getTextId());
			builder.append("\t");
			builder.append(film.getTitle());
			builder.append("\t");
			builder.append(FusionHelper.serializeDate(film.getReleaseDate()));
			builder.append("\t");
			builder.append(FusionHelper.serializeInt(film.getEarnings()));
			builder.append("\t");
			builder.append(FusionHelper.serializeInt(film.getDuration()));
			builder.append("\t");
			builder.append(FusionHelper.serializePairs(film.getLanguages()));
			builder.append("\t");
			builder.append(FusionHelper.serializePairs(film.getCountries()));
			builder.append("\t");
			builder.append(FusionHelper.serializePairs(film.getGenres()));
			builder.append("\n");
		}
		return builder.toString();
	}
}
