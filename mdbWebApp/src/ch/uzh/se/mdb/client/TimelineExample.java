package ch.uzh.se.mdb.client;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.uzh.se.mdb.shared.FilmData;
import ch.uzh.se.mdb.shared.Temp;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataColumn;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.RoleType;
import com.googlecode.gwt.charts.client.format.PatternFormat;
import com.googlecode.gwt.charts.client.timeline.Timeline;
import com.googlecode.gwt.charts.client.timeline.TimelineOptions;

public class TimelineExample extends DockLayoutPanel {
	private Timeline timeline;

	public TimelineExample() {
		 super(Unit.PX);
		initialize();
	}

	private void initialize() {
		
		
		ChartLoader chartLoader = new ChartLoader(ChartPackage.TIMELINE);
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				// Create and attach the chart
				timeline = new Timeline();
				add(timeline);
			}
		});
	}

	public void updateData(Map<String, Temp> countryMap) {
		// Prepare the data
		timeline.clearChart();
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Count");
		dataTable.addColumn(ColumnType.STRING, "Country");
//		dataTable.addColumn(DataColumn.create(ColumnType.STRING, RoleType.TOOLTIP));
		dataTable.addColumn(ColumnType.DATE, "Start");
		dataTable.addColumn(ColumnType.DATE, "End");

		dataTable.addRows(countryMap.size());
		Set<String> keys = countryMap.keySet();
		Iterator<String> it = keys.iterator();
		int start = 0;
		while(it.hasNext()) {
			Temp val = countryMap.get(it.next());
			Date min = val.getStart();
			Date max = val.getEnd();
			if(min.after(max)) {
				min = val.getEnd();
				max = val.getStart();
			}
			dataTable.setValue(start, 0, val.getCountry());
			dataTable.setValue(start, 1, val.getCountry() + "(" + val.getCount() + ")");
//			dataTable.setValue(start, 2, "Total " + val.getCount());
			dataTable.setValue(start, 2, min);
			dataTable.setValue(start, 3, max);
			Logger.getLogger("AAAAAAAA").log(Level.SEVERE, val.getCountry() + "/" + val.getCount() + "/" + min.toString() + "/" + max.toString());
			++start;
		}
		PatternFormat format = PatternFormat.create("{0}<div style=\"color:red; font-style:italic\">{1}</div>");
		int[] columns = new int[countryMap.size()];
		for(int i = 0; i < countryMap.size(); i++) {
			columns[i] = i;
		}
		format.format(dataTable, columns);

		// Set options
		TimelineOptions options = TimelineOptions.create();
		options.setShowRowLabels(true);
		options.setGroupByRowLabel(false);
//		options.setAvoidOverlappingGridLinesl(false);

		// Draw the chart
		timeline.draw(dataTable, options);
	}
}