package ch.uzh.se.mdb.client;

import java.util.List;

import ch.uzh.se.mdb.shared.FilmData;

public class FilmTable {
	private int pageSize = 10;
	private int page;
	private int maxPages;
	private int maxResultRecords;
	
	public FilmTable() {
		page=0;
		maxPages=0;
	}
	
	public void setMaxPages(List<FilmData> movies) {

		if (getOffset() + 100 < maxResultRecords) {
			maxPages = -1;
		} else {

			int tmp = (maxResultRecords / pageSize);
			if (maxResultRecords % pageSize != 0) {
				tmp = tmp + 1;
			}
			maxPages = tmp;
		}
	}
	
	public void reset() {
		page=0;
		maxPages=0;
		maxResultRecords=0;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getPage() {
		return page;
	}

	public int getMaxPages() {
		return maxPages;
	}
	
	public int getMaxResultRecords() {
		return maxResultRecords;
	}

	public void setMaxResultRecords(int maxResultRecords) {
		this.maxResultRecords = maxResultRecords;
	}

	public void flipForward() {
		page = page + 1;
	} 
	
	public void flipBackward() {
		page = (page==0)? 0 : page - 1;
	} 
	
	public int getOffset() {
		return page * pageSize;
	}

}
