package ch.uzh.se.mdb.client;

import java.util.*;

import com.google.gwt.user.client.rpc.*;

import ch.uzh.se.mdb.shared.*;

/**
 * The async counterpart of <code>FilmService</code>.
 * @author Ada
 */
public interface FilmServiceAsync {
	void test(AsyncCallback<String> callback);

	void filter(FilmFilter filter, AsyncCallback<List<FilmData>> callback)
			throws IllegalArgumentException;

	void filter(FilmFilter filter, FilmPaging paging, AsyncCallback<FilmQueryResult> callback)
			throws IllegalArgumentException;

	void count(FilmFilter filter, AsyncCallback<Integer> callback)
			throws IllegalArgumentException;

	void countGroupBy(FilmFilter filter, String groupByField, AsyncCallback<List<FilmCountData>> callback)
			throws IllegalArgumentException;
}

