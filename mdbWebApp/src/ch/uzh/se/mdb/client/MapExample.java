package ch.uzh.se.mdb.client;

import java.util.List;

import ch.uzh.se.mdb.shared.FilmCountData;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.geochart.GeoChart;
import com.googlecode.gwt.charts.client.geochart.GeoChartColorAxis;
import com.googlecode.gwt.charts.client.geochart.GeoChartOptions;
import com.googlecode.gwt.charts.client.options.DisplayMode;

public class MapExample extends DockLayoutPanel {

	private GeoChart chart;

	public MapExample() {
		super(Unit.PX);
		initialize();
	}

	private void initialize() {
		ChartLoader chartLoader = new ChartLoader(ChartPackage.GEOCHART);
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				// Create and attach the chart
				chart = new GeoChart();
				add(chart);
			}
		});
	}

	public void update(List<FilmCountData> countryCounts) {
		chart.clearChart();

		// Create data table.
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Country");
		dataTable.addColumn(ColumnType.NUMBER, "Count");

		// Fill data table with counts.
		for(FilmCountData countryEntry : countryCounts) {
			dataTable.addRow(countryEntry.getValue(), countryEntry.getCount());
		}

		// Set options.
		GeoChartOptions options = GeoChartOptions.create();
		GeoChartColorAxis geoChartColorAxis = GeoChartColorAxis.create();
		geoChartColorAxis.setColors("green", "yellow", "orange");
		options.setColorAxis(geoChartColorAxis);
		options.setDatalessRegionColor("gray");
		options.setDisplayMode(DisplayMode.REGIONS);
		
		// Draw the map.
		chart.draw(dataTable, options);
	}
}