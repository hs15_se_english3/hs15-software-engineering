package ch.uzh.se.mdb.client;

import java.util.*;

import com.google.gwt.user.client.rpc.*;

import ch.uzh.se.mdb.shared.*;

/**
 * The client-side stub for the RPC service.
 * @author Ada
 */
@RemoteServiceRelativePath("film")
public interface FilmService extends RemoteService {
	
	/**
	 * Tests the connection between client and server.
	 */
	String test();
	
	/**
	 * Returns the list of films matching the given filter.
	 */
	List<FilmData> filter(FilmFilter filter)
		throws IllegalArgumentException;

	/**
	 * Returns the list of films matching the given filter and paging.
	 */
	FilmQueryResult filter(FilmFilter filter, FilmPaging paging)
		throws IllegalArgumentException;

	/**
	 * Returns the count of films matching the given filter.
	 */
	public Integer count(FilmFilter filter) 
		throws IllegalArgumentException;
	
	/**
	 * Returns the list of counts of films matching the given filter
	 * grouped by the given field.
	 */
	List<FilmCountData> countGroupBy(FilmFilter filter, String groupByField)
		throws IllegalArgumentException;
}
