package ch.uzh.se.mdb.client;

//<<<<<<< Updated upstream
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ch.uzh.se.mdb.client.widget.slider.RangeSlider;
import ch.uzh.se.mdb.client.widget.slider.Slider;
import ch.uzh.se.mdb.client.widget.slider.SliderEvent;
import ch.uzh.se.mdb.client.widget.slider.SliderListener;
import ch.uzh.se.mdb.shared.FilmCountData;
import ch.uzh.se.mdb.shared.FilmData;
import ch.uzh.se.mdb.shared.FilmFilter;
import ch.uzh.se.mdb.shared.FilmPaging;
import ch.uzh.se.mdb.shared.FilmQueryResult;
import ch.uzh.se.mdb.shared.Temp;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.PieChart;
import com.googlecode.gwt.charts.client.corechart.PieChartOptions;

public class webapp implements EntryPoint, SliderListener {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting
	 * service.
	 */
	private final FilmServiceAsync filmService = GWT.create(FilmService.class);
	private static int page = 0;

	private VerticalPanel mainPanel = new VerticalPanel();
	private VerticalPanel homePanel = new VerticalPanel();
	private VerticalPanel tableMainPanel = new VerticalPanel();
	private HorizontalPanel addPanel = new HorizontalPanel();
	private HorizontalPanel diagramOptionPanel = new HorizontalPanel();

	private Button visualizeTableButton = new Button("Table Visualisation");
	private Button visualizeMapButton = new Button("Map Visualisation");
	private Button visualizeHome = new Button ("Home");
	private Button visualizeDiagramButton = new Button("Genre Distribution Chart");

	private FlexTable movieFlexTable = new FlexTable();

	private TextBox titleFilterBox = new TextBox();
	private IntegerBox releaseDateFromFilterBox = new IntegerBox();
	private IntegerBox releaseDateToFilterBox = new IntegerBox();
	private IntegerBox durationFromFilterBox = new IntegerBox();
	private IntegerBox durationToFilterBox = new IntegerBox();
	private IntegerBox earningsFromFilterBox = new IntegerBox();
	private IntegerBox earningsToFilterBox = new IntegerBox();

	private FlexTable filterTable = new FlexTable();
	private ListBox countryFilterBox = new ListBox();
	private Button applyFilterOptions = new Button("Apply Filter Options");
	private Button resetButton = new Button("Reset Filter");
	private ListBox languageFilterBox = new ListBox();
	private ListBox genreFilterBox = new ListBox();
	private Map<String, String> languageMap = new HashMap<String, String>();
	private Map<String, String> countryMap = new HashMap<String, String>();
	private Map<String, String> genreMap = new HashMap<String, String>();
	private Map<String, String> countryMapDiagram = new HashMap<String, String>();
	private ListBox countryFilterBoxDiagram = new ListBox();
	
	private FlexTable buttonPanel = new FlexTable();
	private Button nextPage = new Button("Next Page");
	private Button previousPage = new Button("Previous Page");

	private Button downloadButton = new Button("Download");
	private Button helpTableButton = new Button("?");
	private Button helpMapButton = new Button("?");
	private Button helpDiagramButton = new Button("?");
	private PopupPanel helpTable= new PopupPanel(true);
	private PopupPanel helpMap= new PopupPanel(true);
	private PopupPanel helpDiagram= new PopupPanel(true);
	
	private VerticalPanel tablePanel = new VerticalPanel();

	/* Integrate timeline panel */
	private TimelineExample timelineExample = new TimelineExample();
	// private MapExample mapExample = new MapExample();
	private MapExample mapExample = new MapExample();
	private VerticalPanel datePanel = new VerticalPanel();
	private VerticalPanel mapPanel = new VerticalPanel();
	private Label m_rangeSliderLabel;
	private RangeSlider m_rangeSlider;
	private Button btnDisplay;
	private int startYear = 2004;
	private int endYear = 2005;
	
	/* Integrate diagram panel */
	private VerticalPanel diagramPanel = new VerticalPanel();
	private Button diagramSelect = new Button("Select a country");
	private Button diagramRemove = new Button("Remove all");
	
	private List<FilmData> movies;
	private FilmTable filmTable = new FilmTable();
	private FilmPaging filmQueryPaging = new FilmPaging(0, 100); 
	
	private DialogBox loadDialog = new DialogBox();
	
	private PieChart chart = new PieChart();

	/**
	 * Entry point method.
	 */
	public void onModuleLoad() {

		
		// Assemble Main panel.
		addPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		mainPanel.setStyleName("PanelForm");
		addPanel.add(visualizeHome);
		visualizeHome.setStyleName("button");
		addPanel.add(visualizeTableButton);
		visualizeTableButton.setStyleName("button");
		addPanel.add(visualizeMapButton);
		visualizeMapButton.setStyleName("button");
		addPanel.add(visualizeDiagramButton);
		visualizeDiagramButton.setStyleName("button");
		mainPanel.add(addPanel);
		addPanel.setStyleName("menuPanel");

		// Add download button and Help Button
		tableMainPanel.setStyleName("PanelForm");
		downloadButton.setStyleName("downloadButton");
		helpTableButton.setStyleName("helpButton");
		tableMainPanel.add(helpTableButton);
		tableMainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		tableMainPanel.add(downloadButton);
		tableMainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		tableMainPanel.setVisible(false);

		// Associate the Main panel with the HTML host page.
		RootPanel.get("movieapp").add(mainPanel);
		RootPanel.get("home").add(homePanel);
		RootPanel.get("table").add(tableMainPanel);
		RootPanel.get("timeline").add(mapPanel);
		RootPanel.get("diagram").add(diagramPanel);
		mainPanel.setSize("100%", "100%");
		homePanel.setSize("100%", "100%");
		tableMainPanel.setSize("100%", "100%");
		mapPanel.setSize("100%", "100%");
		diagramPanel.setSize("100%", "100%");
		
		/* Create homePanel */
		homePanel.setStyleName("PanelForm");
		assembleHome();

		/* Create table panel */
		tablePanel.setStyleName("tablePanel");
		tableMainPanel.add(tablePanel);
		tablePanel.add(filterTable);

		/* Integrate timeline panel */
		mapPanel.setStyleName("PanelForm");
		mapPanel.setSize("100%", "100%");
		mapPanel.add(datePanel);
		datePanel.setSize("100%", "100%");
		mapExample.setStyleName("mapPanelForm");
		helpMapButton.setStyleName("helpButton");
		mapPanel.add(helpMapButton);
		mapPanel.add(mapExample);
		mapPanel.add(timelineExample);
		mapExample.setPixelSize(1400, 600);
		
		/* Integrate ads */

		
		/* Integrate diagram panel */
		diagramPanel.setVisible(false);
		helpDiagramButton.setStyleName("helpButton");
		diagramPanel.add(helpDiagramButton);
		diagramPanel.setStyleName("diagramPanel");

		/*
		 * Create a RangeSlider with: minimum possible value of 1900, maximum
		 * possible value of 2020, default range of 2004 - 2005
		 */
		Label rangeLabel = new Label("Year range:");
		m_rangeSliderLabel = new Label("2004 - 2005");
		m_rangeSliderLabel.addStyleName("slider-values");
		m_rangeSlider = new RangeSlider("range", 1900, 2030, 2004, 2005);
		datePanel.add(rangeLabel);
		datePanel.add(m_rangeSliderLabel);
		HorizontalPanel sliderPanel = new HorizontalPanel();
		datePanel.add(sliderPanel);
		sliderPanel.add(m_rangeSlider);
		m_rangeSlider.setWidth("960px");
		btnDisplay = new Button("Show Movie Count");
		btnDisplay.setStyleName("movieCount");
		sliderPanel.add(btnDisplay);
		sliderPanel.setSpacing(10);
		btnDisplay.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				btnDisplay.setEnabled(false);
				
				FilmFilter filter = constructFilterForTimeline(startYear, endYear);

				// Get films matching filter from service and show them as
				// table.
				filmService.countGroupBy(filter, "Countries", new AsyncCallback<List<FilmCountData>>() {
					public void onFailure(Throwable caught) {
						showError(caught);
						btnDisplay.setEnabled(true);
					}

					public void onSuccess(List<FilmCountData> result) {
						System.out.println("Got data, processing...");
						System.out.println("Updating map...");
						mapExample.update(result);
						System.out.println("Done.");
						btnDisplay.setEnabled(true);
					}
				});
			}
		});
		m_rangeSlider.addListener(this);



		mapPanel.setVisible(false);
		mapPanel.setSpacing(10);
		mapPanel.setSize("100%", "100%");
		mapExample.setPixelSize(1400, 600);
		timelineExample.setPixelSize(1400, 600);

		//Assemble FilterTable
		assembleFilterTable();

		//Fill Genre Filter
		genreFilterBox.addItem("");
		getGenre();

		//Fill Country Filter
		countryFilterBox.addItem("");
		getCountries();

		//Fill Language Filter
		languageFilterBox.addItem("");
		getLanguages();

		/*
		 * Handle Events
		 */
		// Listen for mouse events on the Add button.
		visualizeHome.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				homePanel.setVisible(true);
				tableMainPanel.setVisible(false);
				mapPanel.setVisible(false);
				diagramPanel.setVisible(false);
			}
		});
		visualizeTableButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				homePanel.setVisible(false);
				tableMainPanel.setVisible(true);
				mapPanel.setVisible(false);
				diagramPanel.setVisible(false);
				showDefaultTable();
			}
		});
		// Listen for mouse events on the Map button.
		visualizeMapButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				mapPanel.setVisible(true);
				tableMainPanel.setVisible(false);
				homePanel.setVisible(false);
				diagramPanel.setVisible(false);
				List<FilmCountData> empty = new ArrayList<FilmCountData>();
				mapExample.update(empty);
			}
		});


		/*adding select, remove and country filter to the diagram section */
		diagramOptionPanel.setSpacing(10);
		countryFilterBoxDiagram.addItem("");
		countryFilterBoxDiagram.setStyleName("dropDownBox");
		getCountriesDiagram();
		diagramOptionPanel.add(countryFilterBoxDiagram);
		diagramOptionPanel.add(diagramSelect);
		diagramSelect.setStyleName("optionsButton");
		diagramOptionPanel.add(diagramRemove);
		diagramRemove.setStyleName("optionsButton");
		diagramPanel.add(diagramOptionPanel);
		diagramPanel.setStyleName("PanelForm");
		
		// Listen for mouse events on the Diagram button.
		visualizeDiagramButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				mapPanel.setVisible(false);
				tableMainPanel.setVisible(false);
				diagramPanel.setVisible(true);
				homePanel.setVisible(false);
			}
		});
				
		// Listen for mouse events on the select button by the diagram.
		diagramSelect.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				int index = countryFilterBoxDiagram.getSelectedIndex();
				final String keyFilterDiagram = countryFilterBoxDiagram.getValue(index);
				final String countryFilterDiagram = countryFilterBoxDiagram.getItemText(index);
				if(countryFilterDiagram.equals("") == false) diagramPanel.add(drawChart(keyFilterDiagram, countryFilterDiagram));
				countryFilterBoxDiagram.setItemSelected(0, true);
			}
		});
		
		// Listen for mouse events on the remove button by the diagram.
		diagramRemove.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				diagramPanel.clear();
				diagramPanel.add(helpDiagramButton);
				diagramPanel.add(diagramOptionPanel);
				countryFilterBoxDiagram.setItemSelected(0, true);
			}
		});

		// Listen for mouse events on the ApplyFilterOptions button.
		applyFilterOptions.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				handleNewFilterAction();
			}
		});
		//Listen for mouse events on the ResetButton.
		resetButton.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event){
				showDefaultTable();
			}
		});
		// Listen for keyboard events in the Title input box.
		titleFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		// Listen for keyboard events in the Release Year From input box.
		releaseDateFromFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		// Listen for keyboard events in the Release Year To input box.
		releaseDateToFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		// Listen for keyboard events in the Duration From input box.
		durationFromFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		// Listen for keyboard events in the Duration To input box.
		durationToFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		// Listen for keyboard events in the Earnings From input box.
		earningsFromFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		// Listen for keyboard events in the Earnings To input box.
		earningsToFilterBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleNewFilterAction();
				}
			}
		});
		//Listen for Change in dropdown list Language
		languageFilterBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				handleNewFilterAction();
				
			}
		});
		//Listen for Change in dropdown list Genre
		genreFilterBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				handleNewFilterAction();
			}
		});
		//Listen for Change in dropdown list Country
		countryFilterBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				handleNewFilterAction();
			}
		});
		//Listen for mouse events on the next Page Button
		nextPage.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				flipToNextPage();
			}
		});
		//Listen for mouse events on the previous Page Button
		previousPage.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				flipToPreviousPage();
			}
		});
		
		// Handle download button
		downloadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				// Get current filter.
				FilmFilter filter = constructFilter();
				
				// Launch download URL for current filter.
				String downloadUrl = GWT.getModuleBaseURL() + "download" + getQueryForFilter(filter);
				Window.open(downloadUrl, "_blank", "");
			}
		});
		//Handle Help Button 
		helpTableButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				helpTable.setWidget(new Label("The Table Visualisation shows the movie database in a table. Add a filter to get a list of movies with your searched options only."));
			    helpTable.setPopupPosition(Window.getClientWidth()/ 3, Window.getClientHeight()/3);
				helpTable.show();
				}
		});
		helpMapButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				helpMap.setWidget(new Label("Shows a Map of the amount of movies in a certain timespan. Adjust the timebar to your wishes."));
			    helpMap.setPopupPosition(Window.getClientWidth()/ 3, Window.getClientHeight()/3);
				helpMap.show();
				}
		});
		helpDiagramButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
		        helpDiagram.setWidget(new Label("Shows a Diagram of the genres of a country. Add multiple countries to compare them."));
		        helpDiagram.setPopupPosition(Window.getClientWidth()/ 3, Window.getClientHeight()/3);
		        helpDiagram.show();
				}
		});
}
			
	private void assembleHome(){
		Label label1 = new Label();
		label1.setStyleName("label1");
		Label label2 = new Label();
		label2.setStyleName("label2");
		label1.setText("Welcome to the Movie App - your movie database");
		label2.setText("See your movie information as pie chart, as world map or in a table");
		homePanel.add(label1);
		homePanel.add(label2);
	}

	
	private void executeNewFilterRequest() {
		movies = new ArrayList<FilmData>();
		
		filmQueryPaging = new FilmPaging(0, 100);
		getMoviesForFilter(filmQueryPaging);
	}
	
	private void executeNextPageRequest() {
		// Update paging settings to get next batch of results.
		filmQueryPaging.setOffset(filmQueryPaging.getOffset() + filmQueryPaging.getLimit());
		getMoviesForFilter(filmQueryPaging);
	}
		
	
	private void getMoviesForFilter(FilmPaging paging) {
		FilmFilter filter = constructFilter();

		// Get films matching filter from service and show them as table.
		loadDialog.show();
		filmService.filter(filter, paging, new AsyncCallback<FilmQueryResult>() {
			public void onFailure(Throwable caught) {
				loadDialog.hide();
				showError(caught);
			}

			public void onSuccess(FilmQueryResult result) {
				loadDialog.hide();
				movies.addAll(result.getResultList());
				filmTable.setMaxResultRecords(result.getQueryResultSize());
				updateTable(movies);
			}
		});
	}
	
	
//	private List<FilmData> getFilteredMovies(FilmFilter filter) {
//		filmService.filter(filter, new AsyncCallback<List<FilmData>>() {
//			public void onFailure(Throwable caught) {
//				loadDialog.hide();
//				showError(caught);
//			}
//
//			public void onSuccess(List<FilmData> result) {
//				loadDialog.hide();
//				showNewTableResult(result);
//			}
//		});
//	}
	
	/**
	 * New filter has been entered. 
	 * Get and show new data.
	 */
	private void handleNewFilterAction () {
		movies=null;
		filmTable.reset();
		executeNewFilterRequest();
	}


	private void showDefaultTable() {
		titleFilterBox.setText("");
		releaseDateFromFilterBox.setText("");
		releaseDateToFilterBox.setText("");
		durationFromFilterBox.setText("");
		durationToFilterBox.setText("");
		earningsFromFilterBox.setText("");
		earningsToFilterBox.setText("");
		languageFilterBox.setItemSelected(0, true);
		genreFilterBox.setItemSelected(0, true);
		countryFilterBox.setItemSelected(0, true);
		
		movies = null;
		filmTable.reset();
		executeNewFilterRequest();
	}

	private void flipToPreviousPage() {
		filmTable.flipBackward();
		updateTable(movies);
	}

	private void flipToNextPage() {
		filmTable.flipForward();
		
//		Window.alert("flip. ofs "+ (filmTable.getOffset()+filmTable.getPageSize()) +" - size " + movies.size());
		if( (filmTable.getOffset() + filmTable.getPageSize()) <= movies.size() ) {
			updateTable(movies);
		} else {
			if(movies.size() < filmTable.getMaxResultRecords()) {
			executeNextPageRequest();
			} else {
				updateTable(movies);
			}
		}
			
		
	}
	
	
	private void hideTableAndButtonPanel() {
//		movieFlexTable.setVisible(false);
//		buttonPanel.setVisible(false);
	}
	
	private void showTableAndButtonPanel() {
//		Window.alert("Show Table and button panel");
		tablePanel.add(movieFlexTable);
		tablePanel.add(buttonPanel);
		
		tablePanel.setVisible(true);
		movieFlexTable.setVisible(true);
		buttonPanel.setVisible(true);
	}

	/**
	 * Constructs a <c>FilmFilter</c> from the values of the filter text boxes
	 * and returns it.
	 */
	private FilmFilter constructFilter() {
		// Construct a new filter.
		FilmFilter filter = new FilmFilter();

		// Add the title filter.
		final String titleFilter = titleFilterBox.getText().trim();
		filter.setTitleFilter(titleFilter);

		// Add the country filter.
		int countryIndex = countryFilterBox.getSelectedIndex();
		final String countryFilter = countryFilterBox.getValue(countryIndex);
		filter.setCountryFilter(countryFilter);

		// Add the language filter.
		int languageIndex = languageFilterBox.getSelectedIndex();
		final String languageFilter = languageFilterBox.getValue(languageIndex);
		filter.setLanguageFilter(languageFilter);

		// Add the genre filter.
		int genreIndex = genreFilterBox.getSelectedIndex();
		final String genreFilter = genreFilterBox.getValue(genreIndex);
		filter.setGenreFilter(genreFilter);

		// Add the release year filter
		final Integer releaseDateFromFilter = releaseDateFromFilterBox.getValue();
		filter.setReleaseDateFromFilter(releaseDateFromFilter);
		final Integer releaseDateToFilter = releaseDateToFilterBox.getValue();
		filter.setReleaseDateToFilter(releaseDateToFilter);

		// Add the duration filter.
		final Integer durationFromFilter = durationFromFilterBox.getValue();
		filter.setDurationFromFilter(durationFromFilter);
		final Integer durationToFilter = durationToFilterBox.getValue();
		filter.setDurationToFilter(durationToFilter);

		// Add the earnings filter.
		final Integer earningsFromFilter = earningsFromFilterBox.getValue();
		filter.setEarningsFromFilter(earningsFromFilter);
		final Integer earningsToFilter = earningsToFilterBox.getValue();
		filter.setEarningsToFilter(earningsToFilter);

		// Return the result.
		return filter;
	}


	private FilmFilter constructFilterForTimeline(int startYear, int endYear) {
		// Construct a new filter.
		FilmFilter filter = new FilmFilter();

		// Add the release year filter
		filter.setReleaseDateFromFilter(startYear);
		filter.setReleaseDateToFilter(endYear);

		// Return the result.
		return filter;

	}

	/*
	 * Constructs a query string for the given <c>FilmFilter</c>.
	 */
	private String getQueryForFilter(FilmFilter filter) {
		// Construct query from filter, skipping null or empty values.
		// Do not use UrlBuilder because we cannot parse GWT.getModuleBaseURL().
		return "?"
			+ getStringParameter("titleFilter", filter.getTitleFilter())
			+ getIntegerParameter("releaseDateFromFilter", filter.getReleaseDateFromFilter())
			+ getIntegerParameter("releaseDateToFilter", filter.getReleaseDateToFilter())
			+ getIntegerParameter("durationFromFilter", filter.getDurationFromFilter())
			+ getIntegerParameter("durationToFilter", filter.getDurationToFilter())
			+ getIntegerParameter("earningsFromFilter", filter.getEarningsFromFilter())
			+ getIntegerParameter("earningsToFilter", filter.getEarningsToFilter())
			+ getStringParameter("languageFilter", filter.getLanguageFilter())
			+ getStringParameter("countryFilter", filter.getCountryFilter())
			+ getStringParameter("genreFilter", filter.getGenreFilter());
	}
	
	private String getIntegerParameter(String name, Integer value) {
		return (value == null) ? "" : "&" + name + "=" + URL.encodeQueryString(value.toString());
	}
	
	private String getStringParameter(String name, String value) {
		return (value == null || value.trim().equals("")) ? "" : "&" + name + "=" + URL.encodeQueryString(value);
	}

	/**
	 * Initializes the film table layout.
	 */
	private void initTableLayout() {
		// Clear the table.
		movieFlexTable.removeAllRows();

		// Set up header row.
		movieFlexTable.setText(0, 0, "Name");
		movieFlexTable.setText(0, 1, "Country");
		movieFlexTable.setText(0, 2, "Language");
		movieFlexTable.setText(0, 3, "Genre");
		movieFlexTable.setText(0, 4, "Release Date");
		movieFlexTable.setText(0, 5, "Duration");
		movieFlexTable.setText(0, 6, "Earnings");

		// Add styles to elements in the stock list table.
		movieFlexTable.setCellPadding(6);
		movieFlexTable.getRowFormatter().addStyleName(0, "movieListHeader");
		movieFlexTable.setStyleName("movieList");

		//Add Table to Panel
		tablePanel.add(movieFlexTable);

		buttonPanel.setWidget(0, 0, previousPage);
		buttonPanel.setText(0, 1, "Page: " + (page+1));
		buttonPanel.setWidget(0, 2, nextPage);
		previousPage.setEnabled(false);

		// Add Style components to previous next button
		nextPage.setStyleName("pageButton");
		previousPage.setStyleName("pageButton");

		tablePanel.add(buttonPanel);

	}
	
		private void addRowToMovieTable (FilmData film, int row) {
				DateTimeFormat year = DateTimeFormat.getFormat(PredefinedFormat.YEAR);
				movieFlexTable.setText(row, 0, film.getTitle());
				String countries = String.valueOf(film.getCountries().values());
				countries = countries.substring(1, countries.length()-1);
				movieFlexTable.setText(row, 1, countries);
				String languages = String.valueOf(film.getLanguages().values());
				languages = languages.substring(1, languages.length()-1);
				movieFlexTable.setText(row, 2, languages);
				String genre = String.valueOf(film.getGenres().values());
				genre = genre.substring(1, genre.length()-1);
				movieFlexTable.setText(row, 3, genre);
				String date = String.valueOf(year.format(film.getReleaseDate()));
				if(date == "0" || date =="3870"){
					date="-";
				}
				movieFlexTable.setText(row, 4, date);
				String duration = String.valueOf(film.getDuration());
				if(duration == "0"){
					duration="-";
				}
				movieFlexTable.setText(row, 5, duration);
				String earnings = String.valueOf(film.getEarnings());
				if(earnings=="0"){
					earnings="-";
				}
				movieFlexTable.setText(row, 6, earnings);
			}

	/**
	 * Updates the film table to show the given films.
	 */
	private void updateTable(List<FilmData> films) {
	
		filmTable.setMaxPages(films);
		movieFlexTable.removeAllRows();
		buildMovieTableHeader();
		buildMovieTableNavigation(films);
		movieFlexTable.setStyleName("movieList");
		movieFlexTable.setCellPadding(6);
		
		if (films.size() > 0) {
//			Window.alert("Build table ");
			int offset = filmTable.getPage() * filmTable.getPageSize();
			int max = offset + offset + filmTable.getPageSize();
			for (int i = 0; i < filmTable.getPageSize(); i++) { // result.size()
				addRowToMovieTable(films.get(offset + i), i + 1);
				int row = movieFlexTable.getRowCount();
				movieFlexTable.getCellFormatter().setStyleName(i + 1, 4, "movieListNumericColumn");
				movieFlexTable.getCellFormatter().setStyleName(i + 1, 5, "movieListNumericColumn");
				movieFlexTable.getCellFormatter().setStyleName(i + 1, 6, "movieListNumericColumn");
				movieFlexTable.getRowFormatter().setStyleName(1, "movieFlexTableEvenRow");
				if (row % 2 != 0) {
					movieFlexTable.getRowFormatter().setStyleName(row,"movieFlexTableEvenRow");
					movieFlexTable.getRowFormatter().setStyleName(row-1,"rowStyle");
				}	
			}
		}
//		tablePanel.add(movieFlexTable);
	
		showTableAndButtonPanel();

	}

	private void buildMovieTableHeader() {
		movieFlexTable.setText(0, 0, "Name");
		movieFlexTable.setText(0, 1, "Country");
		movieFlexTable.setText(0, 2, "Language");
		movieFlexTable.setText(0, 3, "Genre");
		movieFlexTable.setText(0, 4, "Release Date");
		movieFlexTable.setText(0, 5, "Duration");
		movieFlexTable.setText(0, 6, "Earnings");
		movieFlexTable.getRowFormatter().setStyleName(0, "movieListHeader");
	}
	
	private void buildMovieTableNavigation(List<FilmData> list) {
		int resultSize = (list != null) ? list.size() : 0;
		String maxPages = (filmTable.getMaxPages() < 0) ? "..." : filmTable.getMaxPages()+"";
//		Window.alert("Result size: " + resultSize);
		buttonPanel.setWidget(0, 0, previousPage);

		String navLabel = "Page: " + (filmTable.getPage() + 1) + "/" + maxPages  + " (" + filmTable.getMaxResultRecords()
				+ " Movies) ";
		if (resultSize == 0) {
			navLabel = "Page: 1/1 (0 Movies) ";
		}

		buttonPanel.setText(0, 1, navLabel);
		buttonPanel.setWidget(0, 2, nextPage);

		// Add Style components to previous next button
		nextPage.setStyleName("pageButton");
		previousPage.setStyleName("pageButton");
		
		if (filmTable.getPage() == 0) {
			previousPage.setEnabled(false);
		} else {
			previousPage.setEnabled(true);
		}
		if(filmTable.getPage() < filmTable.getMaxPages()-1 || filmTable.getMaxPages() < 0) {
			nextPage.setEnabled(true);
		}else {
			nextPage.setEnabled(false);
		}

//		tablePanel.add(buttonPanel);
	}

	/**
	 * Shows error from backend service call as message.
	 */
	private void showError(Throwable caught) {
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Error during test: " + caught.getMessage());
		dialogBox.show();
	}

	@Override
	public void onChange(SliderEvent e) {
		// We don't need to do anything, because everything is done in onSlide
		// in this example
	}

	@Override
	public boolean onSlide(SliderEvent e) {
		Slider source = e.getSource();
		if (source == m_rangeSlider) {
			m_rangeSliderLabel.setText(e.getValues()[0] + " - " + e.getValues()[1]);
			startYear = e.getValues()[0];
			endYear = e.getValues()[1];
		}
		return true;
	}

	@Override
	public void onStart(SliderEvent e) {
		// We are not going to do anything onStart
	}

	@Override
	public void onStop(SliderEvent e) {
		// We are not going to do anything onStop
	}
	/**
	 * Assemble Filter Table
	 */
	public void assembleFilterTable(){
		// Assemble Filter Names
		filterTable.setText(0, 0, "Title:");
		filterTable.setText(0, 1, "Country:");
		filterTable.setText(0, 2, "Language:");
		filterTable.setText(0, 3, "Genre:");
		filterTable.setText(0, 4, "Release year from:");
		filterTable.setText(0, 5, "Release year to:");
		filterTable.setText(0, 6, "Duration from:");
		filterTable.setText(0, 7, "Duration to:");
		filterTable.setText(0, 8, "Earnings from:");
		filterTable.setText(0, 9, "Earnings to:");
		// Assemble Filter Widgets
		filterTable.setWidget(1, 0, titleFilterBox);
		filterTable.setWidget(1, 1, countryFilterBox);
		filterTable.setWidget(1, 2, languageFilterBox);
		filterTable.setWidget(1, 3, genreFilterBox);
		filterTable.setWidget(1, 4, releaseDateFromFilterBox);
		filterTable.setWidget(1, 5, releaseDateToFilterBox);
		filterTable.setWidget(1, 6, durationFromFilterBox);
		filterTable.setWidget(1, 7, durationToFilterBox);
		filterTable.setWidget(1, 8, earningsFromFilterBox);
		filterTable.setWidget(1, 9, earningsToFilterBox);
		filterTable.setWidget(1, 10, applyFilterOptions);
		filterTable.setWidget(1, 11, resetButton);
		// Add Style Components to Widgets
		titleFilterBox.setStyleName("filterName");
		countryFilterBox.setStyleName("dropDownBox");
		languageFilterBox.setStyleName("dropDownBox");
		genreFilterBox.setStyleName("dropDownBox");
		releaseDateFromFilterBox.setStyleName("durationStyle");
		releaseDateToFilterBox.setStyleName("durationStyle");
		durationFromFilterBox.setStyleName("durationStyle");
		durationToFilterBox.setStyleName("durationStyle");
		earningsFromFilterBox.setStyleName("earningsStyle");
		earningsToFilterBox.setStyleName("earningsStyle");
		applyFilterOptions.setStyleName("optionsButton");
		resetButton.setStyleName("resetButton");
		// Add Style Components to Table
		filterTable.getCellFormatter().addStyleName(0, 0, "filterTableStyle");
		filterTable.getCellFormatter().addStyleName(0, 1, "dropDownBox");
		filterTable.getCellFormatter().addStyleName(0, 2, "dropDownBox");
		filterTable.getCellFormatter().addStyleName(0, 3, "dropDownBox");
		filterTable.getCellFormatter().addStyleName(0, 4, "filterTableStyle");
		filterTable.getCellFormatter().addStyleName(0, 5, "filterTableStyle");
		filterTable.getCellFormatter().addStyleName(0, 6, "filterTableStyle");
		filterTable.getCellFormatter().addStyleName(0, 7, "filterTableStyle");
		filterTable.getCellFormatter().addStyleName(0, 8, "filterTableStyle");
		filterTable.getCellFormatter().addStyleName(0, 9, "filterTableStyle");
	}
	/**
	 * Gets the Countries to fill the FilterBox
	 */
	private void getCountries() {
		countryMap.put("/m/0jdd","Afghanistan");
		countryMap.put("/m/0jdx","Albania");
		countryMap.put("/m/0h3y","Algeria");
		countryMap.put("/m/0jgd","Argentina");
		countryMap.put("/m/0jgx","Armenia");
		countryMap.put("/m/0j11","Aruba");
		countryMap.put("/m/0chghy","Australia");
		countryMap.put("/m/0h7x","Austria");
		countryMap.put("/m/0jhd","Azerbaijan");
		countryMap.put("/m/0160w","Bahamas");
		countryMap.put("/m/0161c","Bahrain");
		countryMap.put("/m/0162b","Bangladesh");
		countryMap.put("/m/0154j","Belgium");
		countryMap.put("/m/07bxhl","Bhutan");
		countryMap.put("/m/0165v","Bolivia");
		countryMap.put("/m/0166b","Bosnia and Herzegovina");
		countryMap.put("/m/015fr","Brazil");
		countryMap.put("/m/015qh","Bulgaria");
		countryMap.put("/m/01699","Burkina Faso");
		countryMap.put("/m/04xn_","Burma");
		countryMap.put("/m/01xbgx","Cambodia");
		countryMap.put("/m/01nln","Cameroon");
		countryMap.put("/m/0d060g","Canada");
		countryMap.put("/m/01p1v","Chile");
		countryMap.put("/m/0d05w3","China");
		countryMap.put("/m/01ls2","Colombia");
		countryMap.put("/m/01rxw","Congo");
		countryMap.put("/m/01p8s","Costa Rica");
		countryMap.put("/m/01rbb","Crime");
		countryMap.put("/m/01pj7","Croatia");
		countryMap.put("/m/0d04z6","Cuba");
		countryMap.put("/m/01ppq","Cyprus");
		countryMap.put("/m/01mjq","Czech Republic");
		countryMap.put("/m/01mk6","Czechoslovakia");
		countryMap.put("/m/088xp","Democratic Republic of the Congo");
		countryMap.put("/m/0k6nt","Denmark");
		countryMap.put("/m/02k54","Egypt");
		countryMap.put("/m/02jx1","England");
		countryMap.put("/m/02kmm","Estonia");
		countryMap.put("/m/019pcs","Ethiopia");
		countryMap.put("/m/018j0w","Federal Republic of Yugoslavia");
		countryMap.put("/m/02vzc","Finland");
		countryMap.put("/m/0f8l9c","France");
		countryMap.put("/m/0d0kn","Georgia");
		countryMap.put("/m/0247my","Georgian SSR");
		countryMap.put("/m/03f2w","German Democratic Republic");
		countryMap.put("/m/04306rv","German Language");
		countryMap.put("/m/0345h","Germany");
		countryMap.put("/m/035qy","Greece");
		countryMap.put("/m/03676","Guinea");
		countryMap.put("/m/036b_","Guinea-Bissau");
		countryMap.put("/m/03gyl","Haiti");
		countryMap.put("/m/03h64","Hong Kong");
		countryMap.put("/m/03gj2","Hungary");
		countryMap.put("/m/03rj0","Iceland");
		countryMap.put("/m/03rk0","India");
		countryMap.put("/m/03ryn","Indonesia");
		countryMap.put("/m/03shp","Iran");
		countryMap.put("/m/0d05q4","Iraq");
		countryMap.put("/m/0329by","Iraqi Kurdistan");
		countryMap.put("/m/03rt9","Ireland");
		countryMap.put("/m/03t1s","Isle of Man");
		countryMap.put("/m/03spz","Israel");
		countryMap.put("/m/03rjj","Italy");
		countryMap.put("/m/03_r3","Jamaica");
		countryMap.put("/m/03_3d","Japan");
		countryMap.put("/m/03__y","Jordan");
		countryMap.put("/m/019rg5","Kenya");
		countryMap.put("/m/014tss","Kingdom of Great Britain");
		countryMap.put("/m/02psqkz","Kingdom of Italy");
		countryMap.put("/m/048fz","Korea");
		countryMap.put("/m/047yc","Kuwait");
		countryMap.put("/m/04hqz","Lebanon");
		countryMap.put("/m/04gqr","Libya");
		countryMap.put("/m/04gzd","Lithuania");
		countryMap.put("/m/04g61","Luxembourg");
		countryMap.put("/m/04thp","Macau");
		countryMap.put("/m/0999q","Malayalam Language");
		countryMap.put("/m/09pmkv","Malaysia");
		countryMap.put("/m/04v09","Mali");
		countryMap.put("/m/04v3q","Malta");
		countryMap.put("/m/0h44w","Mandatory Palestine");
		countryMap.put("/m/0b90_r","Mexico");
		countryMap.put("/m/04w58","Monaco");
		countryMap.put("/m/04w8f","Mongolia");
		countryMap.put("/m/056vv","Montenegro");
		countryMap.put("/m/04wgh","Morocco");
		countryMap.put("/m/059z0","Nazi Germany");
		countryMap.put("/m/016zwt","Nepal");
		countryMap.put("/m/059j2","Netherlands");
		countryMap.put("/m/0ctw_b","New Zealand");
		countryMap.put("/m/05cgv","Nigeria");
		countryMap.put("/m/05bcl","Northern Ireland");
		countryMap.put("/m/05b4w","Norway");
		countryMap.put("/m/05sb1","Pakistan");
		countryMap.put("/m/0nyg5","Palestinian territories");
		countryMap.put("/m/0604m","Palestinian Territories");
		countryMap.put("/m/05qx1","Panama");
		countryMap.put("/m/016wzw","Peru");
		countryMap.put("/m/05v8c","Philippines");
		countryMap.put("/m/05qhw","Poland");
		countryMap.put("/m/05r4w","Portugal");
		countryMap.put("/m/05r7t","Puerto Rico");
		countryMap.put("/m/0697s","Qatar");
		countryMap.put("/m/0h66xzc","Republic of China");
		countryMap.put("/m/0bjv6","Republic of Macedonia");
		countryMap.put("/m/06c1y","Romania");
		countryMap.put("/m/06bnz","Russia");
		countryMap.put("/m/06q1r","Scotland");
		countryMap.put("/m/06srk","Senegal");
		countryMap.put("/m/077qn","Serbia");
		countryMap.put("/m/06swt","Serbia and Montenegro");
		countryMap.put("/m/06t2t","Singapore");
		countryMap.put("/m/07hj4m","Slovak Republic");
		countryMap.put("/m/06npd","Slovakia");
		countryMap.put("/m/06t8v","Slovenia");
		countryMap.put("/m/01rhrd","Socialist Federal Republic of Yugoslavia");
		countryMap.put("/m/0hzlz","South Africa");
		countryMap.put("/m/06qd3","South Korea");
		countryMap.put("/m/033fcj","Soviet occupation zone");
		countryMap.put("/m/05vz3zq","Soviet Union");
		countryMap.put("/m/06mkj","Spain");
		countryMap.put("/m/06m_5","Sri Lanka");
		countryMap.put("/m/0d0vqn","Sweden");
		countryMap.put("/m/06mzp","Switzerland");
		countryMap.put("/m/06f32","Taiwan");
		countryMap.put("/m/07f1x","Thailand");
		countryMap.put("/m/07fj_","Tunisia");
		countryMap.put("/m/01znc_","Turkey");
		countryMap.put("/m/01c4pv","Turkmenistan");
		countryMap.put("/m/07t21","Ukraine");
		countryMap.put("/m/0212ny","Ukrainian SSR");
		countryMap.put("/m/0jzptwn","Ukranian SSR");
		countryMap.put("/m/0j1z8","United Arab Emirates");
		countryMap.put("/m/07ssc","United Kingdom");
		countryMap.put("/m/09c7w0","United States of America");
		countryMap.put("/m/07twz","Uruguay");
		countryMap.put("/m/0247pq","Uzbek SSR");
		countryMap.put("/m/07t_x","Uzbekistan");
		countryMap.put("/m/07ylj","Venezuela");
		countryMap.put("/m/01crd5","Vietnam");
		countryMap.put("/m/0j5g9","Wales");
		countryMap.put("/m/084n_","Weimar Republic");
		countryMap.put("/m/082fr","West Germany");
		countryMap.put("/m/087vz","Yugoslavia");
		countryMap.put("/m/088vb","Zambia");
		countryMap.put("/m/088q4","Zimbabwe");

		for(Entry<String, String> entry : countryMap.entrySet()) {
			countryFilterBox.addItem(entry.getValue(), entry.getKey());
		}
	}
	/**
	 * Gets the Languages to fill the filterBox
	 */
	private void getLanguages() {
		languageMap.put("/m/04167hr","Aboriginal Malay languages");
		languageMap.put("/m/0x82","Afrikaans Language");
		languageMap.put("/m/02hw_03","Akan Language");
		languageMap.put("/m/012v8","Albanian language");
		languageMap.put("/m/021s0r","Algonquin Language");
		languageMap.put("/m/0t_2","American English");
		languageMap.put("/m/0my5","American Sign Language");
		languageMap.put("/m/0nj_l","Amharic Language");
		languageMap.put("/m/01307m","Ancient Greek");
		languageMap.put("/m/0jzc","Arabic Language");
		languageMap.put("/m/0ycv","Aramaic language");
		languageMap.put("/m/0xrg","Armenian Language");
		languageMap.put("/m/019vzt","Assamese Language");
		languageMap.put("/m/04w042","Assyrian Neo-Aramaic Language");
		languageMap.put("/m/0v0s","Australian English");
		languageMap.put("/m/04ng64","Awadhi Language");
		languageMap.put("/m/01bcw_","Azerbaijani language");
		languageMap.put("/m/01gdq","Bambara language");
		languageMap.put("/m/0c99n","Belarusian language");
		languageMap.put("/m/01c7y","Bengali Language");
		languageMap.put("/m/02dn30","Bhojpuri Language");
		languageMap.put("/m/01841n","Bosnian language");
		languageMap.put("/m/02zj58","Brazilian Portuguese");
		languageMap.put("/m/01bkv","Bulgarian Language");
		languageMap.put("/m/01xrrm","Burmese Language");
		languageMap.put("/m/012w70","Cantonese");
		languageMap.put("/m/01m69","Catalan language");
		languageMap.put("/m/01zfd7","Cebuano language");
		languageMap.put("/m/02q_yfb","Chadian Arabic");
		languageMap.put("/m/02fpb9","Chewa language");
		languageMap.put("/m/01d4zk","Cheyenne Language");
		languageMap.put("/m/03dfwj","Chhattisgarhi Language");
		languageMap.put("/m/01r2l","Chinese language");
		languageMap.put("/m/0dxj6","Chinese, Hakka Language");
		languageMap.put("/m/0311f_","Chinese, Jinyu Language");
		languageMap.put("/m/02nw1m","Classical Arabic");
		languageMap.put("/m/0247v","Corsican Language");
		languageMap.put("/m/0dqhd","Cree language");
		languageMap.put("/m/0k0sv","Croatian language");
		languageMap.put("/m/01wgr","Czech Language");
		languageMap.put("/m/0295r","Danish Language");
		languageMap.put("/m/09w_t","Dari");
		languageMap.put("/m/02bv9","Dutch Language");
		languageMap.put("/m/020gly","Dzongkha Language");
		languageMap.put("/m/034f9_","Egyptian Arabic");
		languageMap.put("/m/02hxr14","Egyptian, Ancient");
		languageMap.put("/m/02h40lc","English Language");
		languageMap.put("/m/02s2l","Estonian Language");
		languageMap.put("/m/02hwm04","Farsi, Western Language");
		languageMap.put("/m/01jb8r","Filipino language");
		languageMap.put("/m/01gp_d","Finnish Language");
		languageMap.put("/m/049dc8","Flemish language");
		languageMap.put("/m/0f8l9c","France");
		languageMap.put("/m/064_8sq","French Language");
		languageMap.put("/m/021wr6","French Sign Language");
		languageMap.put("/m/05n7jk","Fula language");
		languageMap.put("/m/02hwsmw","Fulfulde, Adamawa Language");
		languageMap.put("/m/01ncmg","Gaelic");
		languageMap.put("/m/014267","Georgian Language");
		languageMap.put("/m/04306rv","German Language");
		languageMap.put("/m/0349s","Greek Language");
		languageMap.put("/m/0121sr","Gujarati Language");
		languageMap.put("/m/02hwgqf","Gumatj Language");
		languageMap.put("/m/02hwsp1","Haryanvi Language");
		languageMap.put("/m/03ph1","Hausa Language");
		languageMap.put("/m/03pmt","Hawaiian language");
		languageMap.put("/m/0177wc","Hazaragi Language");
		languageMap.put("/m/03hkp","Hebrew Language");
		languageMap.put("/m/032db0","Hiligaynon language");
		languageMap.put("/m/03k50","Hindi Language");
		languageMap.put("/m/0hlyy","Hindustani language");
		languageMap.put("/m/025fqg","Hinglish");
		languageMap.put("/m/04j9zqq","Hokkien");
		languageMap.put("/m/02ztjwg","Hungarian language");
		languageMap.put("/m/03gj2","Hungary");
		languageMap.put("/m/01gmkg","Icelandic Language");
		languageMap.put("/m/01lz87","Indian English");
		languageMap.put("/m/097kp","Indonesian Language");
		languageMap.put("/m/0322q8","Inuktitut");
		languageMap.put("/m/03x42","Irish");
		languageMap.put("/m/09y2k2","Italian");
		languageMap.put("/m/02bjrlw","Italian Language");
		languageMap.put("/m/03_9r","Japanese Language");
		languageMap.put("/m/09bnf","Kannada Language");
		languageMap.put("/m/01lqm","Khmer language");
		languageMap.put("/m/02hx_0t","Khmer, Central Language");
		languageMap.put("/m/01xls2","Kinyarwanda language");
		languageMap.put("/m/02hwhyv","Korean Language");
		languageMap.put("/m/05ks5d","Krio Language");
		languageMap.put("/m/0bv_f3s","Kriolu");
		languageMap.put("/m/0ccy2f","Kuna language");
		languageMap.put("/m/0b1g2","Kurdish language");
		languageMap.put("/m/04h9h","Latin Language");
		languageMap.put("/m/0k7zj","Lithuanian language");
		languageMap.put("/m/04mz4","Luxembourgish language");
		languageMap.put("/m/0dds9","Maori language");
		languageMap.put("/m/04t67","Macedonian Language");
		languageMap.put("/m/0y1mh","Malay Language");
		languageMap.put("/m/0999q","Malayalam Language");
		languageMap.put("/m/03115z","Mandarin Chinese");
		languageMap.put("/m/0d8x9x","Maninka language");
		languageMap.put("/m/055qm","Marathi Language");
		languageMap.put("/m/03vsxc","Maya, Yucatan Language");
		languageMap.put("/m/02hwtky","Mende Language");
		languageMap.put("/m/01c44b","Min Nan");
		languageMap.put("/m/018tw7","Mohawk Language");
		languageMap.put("/m/0148c4","Mongolian language");
		languageMap.put("/m/05hjc","Nahuatl languages");
		languageMap.put("/m/017yph","Navajo Language");
		languageMap.put("/m/0h0t4","Nepali Language");
		languageMap.put("/m/05f_3","Norwegian Language");
		languageMap.put("/m/01y6qp","Oriya Language");
		languageMap.put("/m/01_5gm","Papiamento language");
		languageMap.put("/m/0swlx","Pashto language");
		languageMap.put("/m/06dqgy","Pawnee Language");
		languageMap.put("/m/032f6","Persian Language");
		languageMap.put("/m/02drlb","Picard Language");
		languageMap.put("/m/018t2j","Plautdietsch Language");
		languageMap.put("/m/05qqm","Polish Language");
		languageMap.put("/m/05zjd","Portuguese Language");
		languageMap.put("/m/0688f","Punjabi language");
		languageMap.put("/m/06b4d","Quechua");
		languageMap.put("/m/05xjzj","Rajasthani language");
		languageMap.put("/m/012psb","Romani language");
		languageMap.put("/m/02hxc3j","Romanian Language");
		languageMap.put("/m/06b_j","Russian Language");
		languageMap.put("/m/04d36l","Saami, North Language");
		languageMap.put("/m/034_x0","Sami languages");
		languageMap.put("/m/06x06","Sanskrit Language");
		languageMap.put("/m/02hw_s_","Scanian Language");
		languageMap.put("/m/0k0sb","Serbian language");
		languageMap.put("/m/06x8y","Serbo-Croatian");
		languageMap.put("/m/02k30q","Shanghainese");
		languageMap.put("/m/02c_bf","Sicilian Language");
		languageMap.put("/m/06ppq","Silent film");
		languageMap.put("/m/06zvx","Slovak Language");
		languageMap.put("/m/06zvd","Slovenian language");
		languageMap.put("/m/028xxn","Somali Language");
		languageMap.put("/m/01vp3n","Sotho language");
		languageMap.put("/m/0c2j95","Southwestern Mandarin");
		languageMap.put("/m/06nm1","Spanish Language");
		languageMap.put("/m/0459q4","Standard Cantonese");
		languageMap.put("/m/0653m","Standard Mandarin");
		languageMap.put("/m/064r7fk","Standard Tibetan");
		languageMap.put("/m/071fb","Swahili Language");
		languageMap.put("/m/06mp7","Swedish Language");
		languageMap.put("/m/06rt9","Swiss German Language");
		languageMap.put("/m/07qv_","Tagalog language");
		languageMap.put("/m/0g1jw","Taiwanese");
		languageMap.put("/m/095hnm","Tamang language");
		languageMap.put("/m/07c9s","Tamil Language");
		languageMap.put("/m/09s02","Telugu language");
		languageMap.put("/m/045k00","Teochew");
		languageMap.put("/m/0c_v2","Thai Language");
		languageMap.put("/m/02hx88","Thai, Northeastern Language");
		languageMap.put("/m/01kbdv","Tibetan languages");
		languageMap.put("/m/01l1db","Tulu Language");
		languageMap.put("/m/02hwyss","Turkish Language");
		languageMap.put("/m/0cjk9","Ukrainian Language");
		languageMap.put("/m/02hxcvy","Urdu Language");
		languageMap.put("/m/07zrf","Vietnamese Language");
		languageMap.put("/m/083tk","Welsh Language");
		languageMap.put("/m/02hwrpy","Wolof Language");
		languageMap.put("/m/0nl8g","Xhosa Language");
		languageMap.put("/m/0880p","Yiddish Language");
		languageMap.put("/m/0fclmt","Yolngu Matha");
		languageMap.put("/m/0gndv","Zulu Language");

		for(Entry<String, String> entry : languageMap.entrySet()) {
			languageFilterBox.addItem(entry.getValue(), entry.getKey());
		}
	}

	/**
	 * Gets the genres to fill the GenreBox
	 */
	private void getGenre() {
		genreMap.put("/m/01hwc6","Absurdism");
		genreMap.put("/m/02rjkzy","Acid western");
		genreMap.put("/m/02kdv5l","Action");
		genreMap.put("/m/0hj3l_y","Action Comedy");
		genreMap.put("/m/0cq22f9","Action Thrillers");
		genreMap.put("/m/03btsm8","Action/Adventure");
		genreMap.put("/m/0hj3mry","Addiction Drama");
		genreMap.put("/m/02nxfrh","Adult");
		genreMap.put("/m/03k9fj","Adventure");
		genreMap.put("/m/0hj3msd","Adventure Comedy");
		genreMap.put("/m/01xpvb7","Airplanes and airports");
		genreMap.put("/m/08w0_f","Albino bias");
		genreMap.put("/m/0hj3mt0","Alien Film");
		genreMap.put("/m/0l13q","Alien invasion");
		genreMap.put("/m/09n5t_","Americana");
		genreMap.put("/m/0hj3mtj","Animal Picture");
		genreMap.put("/m/0hj3mtp","Animals");
		genreMap.put("/m/095bb","Animated cartoon");
		genreMap.put("/m/0hj3mtv","Animated Musical");
		genreMap.put("/m/0hcr","Animation");
		genreMap.put("/m/0jxy","Anime");
		genreMap.put("/m/01tz3c","Anthology");
		genreMap.put("/m/0hj3mt_","Anthropology");
		genreMap.put("/m/01fc50","Anti-war");
		genreMap.put("/m/07k67c","Anti-war film");
		genreMap.put("/m/0hc1z","Apocalyptic and post-apocalyptic fiction");
		genreMap.put("/m/03jp5h0","Archaeology");
		genreMap.put("/m/03jp56q","Archives and records");
		genreMap.put("/m/02n4lw","Art film");
		genreMap.put("/m/0ltv","Auto racing");
		genreMap.put("/m/0k345","Avant-garde");
		genreMap.put("/m/0hj3mvs","Backstage Musical");
		genreMap.put("/m/018jz","Baseball");
		genreMap.put("/m/0hj3mw3","Beach Film");
		genreMap.put("/m/07ftpb","Beach Party film");
		genreMap.put("/m/0n6n298","Bengali Cinema");
		genreMap.put("/m/0hj3mwf","Biker Film");
		genreMap.put("/m/03bxz7","Biographical film");
		genreMap.put("/m/017fp","Biography");
		genreMap.put("/m/0hj3mws","Biopic [feature]");
		genreMap.put("/m/0vgkd","Black comedy");
		genreMap.put("/m/01g6gs","Black-and-white");
		genreMap.put("/m/018td","Blaxploitation");
		genreMap.put("/m/0hj3mwy","Bloopers & Candid Camera");
		genreMap.put("/m/0qdzd","B-movie");
		genreMap.put("/m/01chg","Bollywood");
		genreMap.put("/m/01cgz","Boxing");
		genreMap.put("/m/017g5","Breakdance");
		genreMap.put("/m/0hj3mx2","British Empire Film");
		genreMap.put("/m/06_vsc","British New Wave");
		genreMap.put("/m/0572q3","Bruceploitation");
		genreMap.put("/m/0399b7","Buddy cop");
		genreMap.put("/m/0556j8","Buddy film");
		genreMap.put("/m/0jmw6g2","Buddy Picture");
		genreMap.put("/m/0hj3mx7","Business");
		genreMap.put("/m/0hj3mvm","B-Western");
		genreMap.put("/m/0lf45","Camp");
		genreMap.put("/m/016vh2","Caper story");
		genreMap.put("/m/0hj3mxk","Cavalry Film");
		genreMap.put("/m/0hj3mxr","Chase Movie");
		genreMap.put("/m/02crcx","Chick flick");
		genreMap.put("/m/0hj3mxx","Childhood Drama");
		genreMap.put("/m/0bj8m2","Children's");
		genreMap.put("/m/0hj3my6","Children's Entertainment");
		genreMap.put("/m/0hj3myc","Children's Fantasy");
		genreMap.put("/m/0hj3myj","Children's Issues");
		genreMap.put("/m/0hj3myq","Children's/Family");
		genreMap.put("/m/0gw5qqq","Chinese Movies");
		genreMap.put("/m/04p5fxn","Christian film");
		genreMap.put("/m/0bwgnb","Christmas movie");
		genreMap.put("/m/02fcct","Clay animation");
		genreMap.put("/m/075qx_b","C-Movie");
		genreMap.put("/m/01w1sx","Cold War");
		genreMap.put("/m/0hj3mz0","Combat Films");
		genreMap.put("/m/0bbctgg","Comdedy");
		genreMap.put("/m/01z4y","Comedy");
		genreMap.put("/m/05p553","Comedy film");
		genreMap.put("/m/04q6sch","Comedy horror");
		genreMap.put("/m/0hj3mz5","Comedy of Errors");
		genreMap.put("/m/03p5xs","Comedy of manners");
		genreMap.put("/m/0hj3mzb","Comedy Thriller");
		genreMap.put("/m/0hj3mzj","Comedy Western");
		genreMap.put("/m/01t_vv","Comedy-drama");
		genreMap.put("/m/01j1n2","Coming of age");
		genreMap.put("/m/0g9zb16","Coming-of-age film");
		genreMap.put("/m/01zhp","Computer Animation");
		genreMap.put("/m/0hj3mzv","Computers");
		genreMap.put("/m/0d2rhq","Concert film");
		genreMap.put("/m/0594kx","Conspiracy fiction");
		genreMap.put("/m/0hj3m_6","Costume Adventure");
		genreMap.put("/m/04xvh5","Costume drama");
		genreMap.put("/m/0hj3m_c","Costume Horror");
		genreMap.put("/m/0hj3m_k","Courtroom Comedy");
		genreMap.put("/m/05bh16v","Courtroom Drama");
		genreMap.put("/m/0hj3m_q","Creature Film");
		genreMap.put("/m/01rbb","Crime");
		genreMap.put("/m/0hj3m_x","Crime Comedy");
		genreMap.put("/m/0hj3n01","Crime Drama");
		genreMap.put("/m/0lsxr","Crime Fiction");
		genreMap.put("/m/02wtdps","Crime Thriller");
		genreMap.put("/m/01q03","Cult");
		genreMap.put("/m/0hj3n07","Culture & Society");
		genreMap.put("/m/01qpc","Cyberpunk");
		genreMap.put("/m/01xtwm","Czechoslovak New Wave");
		genreMap.put("/m/04dn71w","Dance");
		genreMap.put("/m/0g9psvk","Demonic child");
		genreMap.put("/m/0vjs6","Detective");
		genreMap.put("/m/028v3","Detective fiction");
		genreMap.put("/m/01drsx","Disaster");
		genreMap.put("/m/01f9r0","Docudrama");
		genreMap.put("/m/0jtdp","Documentary");
		genreMap.put("/m/02gk5","Dogme 95");
		genreMap.put("/m/0hj3n0k","Domestic Comedy");
		genreMap.put("/m/094ddt","Doomsday film");
		genreMap.put("/m/07s9rl0","Drama");
		genreMap.put("/m/026ny","Dystopia");
		genreMap.put("/m/04gghpk","Ealing Comedies");
		genreMap.put("/m/0hj3n0q","Early Black Cinema");
		genreMap.put("/m/02jfc","Education");
		genreMap.put("/m/04zgj2","Educational");
		genreMap.put("/m/0hj3n0w","Ensemble Film");
		genreMap.put("/m/0hj3n11","Environmental Science");
		genreMap.put("/m/06l3bl","Epic");
		genreMap.put("/m/0253g1","Epic Western");
		genreMap.put("/m/0hj3n16","Erotic Drama");
		genreMap.put("/m/0glj9q","Erotic thriller");
		genreMap.put("/m/02js9","Erotica");
		genreMap.put("/m/0hj3n1c","Escape Film");
		genreMap.put("/m/0hj3n1j","Essay Film");
		genreMap.put("/m/02m4t","Existentialism");
		genreMap.put("/m/0424mc","Experimental film");
		genreMap.put("/m/02xh8t","Exploitation");
		genreMap.put("/m/0pybl","Expressionism");
		genreMap.put("/m/0hj3n1x","Extreme Sports");
		genreMap.put("/m/0bxg3","Fairy tale");
		genreMap.put("/m/0hj3n21","Family & Personal Relationships");
		genreMap.put("/m/02h8pkk","Family Drama");
		genreMap.put("/m/0hqxf","Family Film");
		genreMap.put("/m/0hj3n26","Family-Oriented Adventure");
		genreMap.put("/m/068twy","Fan film");
		genreMap.put("/m/01hmnh","Fantasy");
		genreMap.put("/m/0hj3n2k","Fantasy Adventure");
		genreMap.put("/m/0hj3n2s","Fantasy Comedy");
		genreMap.put("/m/0hj3n2_","Fantasy Drama");
		genreMap.put("/m/01hw54","Feature film");
		genreMap.put("/m/06w2n3t","Female buddy film");
		genreMap.put("/m/0hj3n34","Feminist Film");
		genreMap.put("/m/0604r_","Fictional film");
		genreMap.put("/m/0j1d47h","Filipino");
		genreMap.put("/m/0gxblw4","Filipino Movies");
		genreMap.put("/m/0hj3n3h","Film");
		genreMap.put("/m/0hj3n3b","Film & Television History");
		genreMap.put("/m/09glv8x","Film a clef");
		genreMap.put("/m/060__y","Film adaptation");
		genreMap.put("/m/02xh1","Film noir");
		genreMap.put("/m/0hj3n3v","Filmed Play");
		genreMap.put("/m/0hj3n3n","Film-Opera");
		genreMap.put("/m/0hj3n40","Finance & Investing");
		genreMap.put("/m/01j6zc","Foreign legion");
		genreMap.put("/m/0279xh5","Future noir");
		genreMap.put("/m/0gw5w78","Gangster Film");
		genreMap.put("/m/0d63kt","Gay");
		genreMap.put("/m/0bc42t_","Gay Interest");
		genreMap.put("/m/069x_s","Gay pornography");
		genreMap.put("/m/04tkhfk","Gay Themed");
		genreMap.put("/m/0hj3n4b","Gender Issues");
		genreMap.put("/m/05tnmt","Giallo");
		genreMap.put("/m/0hj3n4h","Glamorized Spy Film");
		genreMap.put("/m/02rd8h3","Goat gland");
		genreMap.put("/m/0hj3n4p","Gothic Film");
		genreMap.put("/m/0hj3n4v","Graphic & Applied Arts");
		genreMap.put("/m/0bbc17","Gross out");
		genreMap.put("/m/01jw2w","Gross-out film");
		genreMap.put("/m/0kpq_x","Gulf War");
		genreMap.put("/m/0hj3n57","Hagiography");
		genreMap.put("/m/01pxyy","Hardcore pornography");
		genreMap.put("/m/0hj3n5l","Haunted House Film");
		genreMap.put("/m/0hj3n5r","Health & Fitness");
		genreMap.put("/m/0hj3n5y","Heaven-Can-Wait Fantasies");
		genreMap.put("/m/0hj3n62","Heavenly Comedy");
		genreMap.put("/m/04btyz","Heist");
		genreMap.put("/m/026v1nw","Hip hop movies");
		genreMap.put("/m/0cq22ds","Historical Documentaries");
		genreMap.put("/m/03hn0","Historical drama");
		genreMap.put("/m/0hj3n6f","Historical Epic");
		genreMap.put("/m/02p0szs","Historical fiction");
		genreMap.put("/m/03g3w","History");
		genreMap.put("/m/0hj3n6r","Holiday Film");
		genreMap.put("/m/03npn","Horror");
		genreMap.put("/m/0hj3n7f","Horror Comedy");
		genreMap.put("/m/0jxxt","Horse racing");
		genreMap.put("/m/09kqc","Humour");
		genreMap.put("/m/0hj3n7m","Hybrid Western");
		genreMap.put("/m/0hj3n7s","Illnesses & Disabilities");
		genreMap.put("/m/0hj3n7y","Indian Western");
		genreMap.put("/m/0219x_","Indie");
		genreMap.put("/m/0hj3n84","Inspirational Drama");
		genreMap.put("/m/0hj3n89","Instrumental Music");
		genreMap.put("/m/0hj3n8h","Interpersonal Relationships");
		genreMap.put("/m/0hj3n8q","Inventions & Innovations");
		genreMap.put("/m/0gw5n2f","Japanese Movies");
		genreMap.put("/m/0hj3n91","Journalism");
		genreMap.put("/m/0ftnl_","Jukebox musical");
		genreMap.put("/m/0hj3n96","Jungle Film");
		genreMap.put("/m/0hj3n9c","Juvenile Delinquency Film");
		genreMap.put("/m/04tk13","Kafkaesque");
		genreMap.put("/m/04fy52","Kitchen sink realism");
		genreMap.put("/m/0hj3n9k","Language & Literature");
		genreMap.put("/m/03bt31n","Latino");
		genreMap.put("/m/0hj3n9r","Law & Crime");
		genreMap.put("/m/0gs6m","Legal drama");
		genreMap.put("/m/0hn10","LGBT");
		genreMap.put("/m/03jp5g4","Libraries and librarians");
		genreMap.put("/m/0hj3nb2","Linguistics");
		genreMap.put("/m/0c031k6","Live action");
		genreMap.put("/m/0n6m8vw","Malayalam Cinema");
		genreMap.put("/m/0hj3nbk","Marriage Drama");
		genreMap.put("/m/04t2t","Martial Arts Film");
		genreMap.put("/m/0hj3nbs","Master Criminal Films");
		genreMap.put("/m/0hj3nby","Media Satire");
		genreMap.put("/m/0hj3nc2","Media Studies");
		genreMap.put("/m/04gm78f","Medical fiction");
		genreMap.put("/m/01lrrt","Melodrama");
		genreMap.put("/m/0l4h_","Mockumentary");
		genreMap.put("/m/04dnp5","Mondo film");
		genreMap.put("/m/0h9qh","Monster");
		genreMap.put("/m/0g092b","Monster movie");
		genreMap.put("/m/0949yv","Movie serial");
		genreMap.put("/m/01xpr_s","Movies About Gladiators");
		genreMap.put("/m/02qyzxg","Mumblecore");
		genreMap.put("/m/04rlf","Music");
		genreMap.put("/m/04t36","Musical");
		genreMap.put("/m/0220p9g","Musical comedy");
		genreMap.put("/m/02wtdkf","Musical Drama");
		genreMap.put("/m/02n4kr","Mystery");
		genreMap.put("/m/0hh_j8m","Mythological Fantasy");
		genreMap.put("/m/0g2k1","Natural disaster");
		genreMap.put("/m/06vxwl5","Natural horror films");
		genreMap.put("/m/05h0n","Nature");
		genreMap.put("/m/03j0dp","Neo-noir");
		genreMap.put("/m/0955rx","Neorealism");
		genreMap.put("/m/03pgfj","New Hollywood");
		genreMap.put("/m/022wy7","New Queer Cinema");
		genreMap.put("/m/05jhg","News");
		genreMap.put("/m/03gjpn8","Ninja movie");
		genreMap.put("/m/026zf_v","Northern");
		genreMap.put("/m/096h3","Nuclear warfare");
		genreMap.put("/m/09swn","Operetta");
		genreMap.put("/m/01nfk7","Outlaw");
		genreMap.put("/m/02prgwj","Outlaw biker film");
		genreMap.put("/m/02r5rng","Parkour in popular culture");
		genreMap.put("/m/0gf28","Parody");
		genreMap.put("/m/05spfln","Patriotic film");
		genreMap.put("/m/05bdtfl","Period Horror");
		genreMap.put("/m/04xvlr","Period piece");
		genreMap.put("/m/04k2v3","Pinku eiga");
		genreMap.put("/m/03p15s3","Plague");
		genreMap.put("/m/05svk","Point of view shot");
		genreMap.put("/m/0cshrf","Political cinema");
		genreMap.put("/m/0bx6ch9","Political Documetary");
		genreMap.put("/m/03mqtr","Political drama");
		genreMap.put("/m/05mrx8","Political satire");
		genreMap.put("/m/02qfv5d","Political thriller");
		genreMap.put("/m/01jk9n","Pornographic movie");
		genreMap.put("/m/0f0jjz","Pornography");
		genreMap.put("/m/087lqx","Pre-Code");
		genreMap.put("/m/0d1w9","Prison");
		genreMap.put("/m/0b5_s_","Prison escape");
		genreMap.put("/m/05c3mp2","Prison film");
		genreMap.put("/m/01p3vp","Private military company");
		genreMap.put("/m/05swd","Propaganda film");
		genreMap.put("/m/05c4g7","Psycho-biddy");
		genreMap.put("/m/01zxzp","Psychological horror");
		genreMap.put("/m/09blyk","Psychological thriller");
		genreMap.put("/m/05r6t","Punk rock");
		genreMap.put("/m/0bxdv5","Race movie");
		genreMap.put("/m/023pxm","Reboot");
		genreMap.put("/m/02djzlc","Religious Film");
		genreMap.put("/m/0jdm8","Remake");
		genreMap.put("/m/018sjn","Revenge");
		genreMap.put("/m/0h1l_48","Revisionist Fairy Tale");
		genreMap.put("/m/04cb4x","Revisionist Western");
		genreMap.put("/m/04228s","Road movie");
		genreMap.put("/m/0b7z1vt","Road-Horror");
		genreMap.put("/m/0520lz","Roadshow theatrical release");
		genreMap.put("/m/05bg8cj","Roadshow/Carny");
		genreMap.put("/m/01j28z","Rockumentary");
		genreMap.put("/m/02l7c8","Romance Film");
		genreMap.put("/m/06cvj","Romantic comedy");
		genreMap.put("/m/068d7h","Romantic drama");
		genreMap.put("/m/03rzvv","Romantic fantasy");
		genreMap.put("/m/0g9yrf1","Romantic thriller");
		genreMap.put("/m/02qqtlj","Samurai cinema");
		genreMap.put("/m/06nbt","Satire");
		genreMap.put("/m/09tvt3","School story");
		genreMap.put("/m/0g576c","Sci Fi Pictures original films");
		genreMap.put("/m/06n90","Science Fiction");
		genreMap.put("/m/07y0lv","Science fiction Western");
		genreMap.put("/m/0cq23f0","Sci-Fi Adventure");
		genreMap.put("/m/0cq22z7","Sci-Fi Horror");
		genreMap.put("/m/01qs3y7","Sci-Fi Thriller");
		genreMap.put("/m/06qm3","Screwball comedy");
		genreMap.put("/m/0gsy3b","Sex comedy");
		genreMap.put("/m/06b0n3","Sexploitation");
		genreMap.put("/m/02hmvc","Short Film");
		genreMap.put("/m/06ppq","Silent film");
		genreMap.put("/m/0dgvt5","Silhouette animation");
		genreMap.put("/m/0bps_n","Singing cowboy");
		genreMap.put("/m/09q17","Slapstick");
		genreMap.put("/m/01585b","Slasher");
		genreMap.put("/m/0clz1b","Slice of life story");
		genreMap.put("/m/075fzd","Social issues");
		genreMap.put("/m/06lbpz","Social problem film");
		genreMap.put("/m/01yldk","Softcore Porn");
		genreMap.put("/m/070yc","Space opera");
		genreMap.put("/m/09zvmj","Space western");
		genreMap.put("/m/0771_","Spaghetti Western");
		genreMap.put("/m/02vn3k","Splatter film");
		genreMap.put("/m/02rcm2r","Sponsored film");
		genreMap.put("/m/01z02hx","Sports");
		genreMap.put("/m/0bkbm","Spy");
		genreMap.put("/m/0q00t","Stand-up comedy");
		genreMap.put("/m/08kh3l","Star vehicle");
		genreMap.put("/m/06ymb","Statutory rape");
		genreMap.put("/m/06www","Steampunk");
		genreMap.put("/m/03v6qx","Stoner film");
		genreMap.put("/m/06qln","Stop motion");
		genreMap.put("/m/0btmb","Superhero");
		genreMap.put("/m/04pbhw","Superhero movie");
		genreMap.put("/m/014g0t","Supermarionation");
		genreMap.put("/m/0fdjb","Supernatural");
		genreMap.put("/m/073_6","Surrealism");
		genreMap.put("/m/0c3351","Suspense");
		genreMap.put("/m/02qvnvs","Swashbuckler films");
		genreMap.put("/m/01lmb_","Sword and Sandal");
		genreMap.put("/m/0dz8b","Sword and sorcery");
		genreMap.put("/m/0520_cy","Sword and sorcery films");
		genreMap.put("/m/0fztc6","Tamil cinema");
		genreMap.put("/m/02b5_l","Teen");
		genreMap.put("/m/015w9s","Television movie");
		genreMap.put("/m/02z8vz","The Netherlands in World War II");
		genreMap.put("/m/06pqf08","Therimin music");
		genreMap.put("/m/01jfsb","Thriller");
		genreMap.put("/m/07s2s","Time travel");
		genreMap.put("/m/02z0t_","Tokusatsu");
		genreMap.put("/m/02cqmh","Tollywood");
		genreMap.put("/m/0fx2s","Tragedy");
		genreMap.put("/m/0q9mp","Tragicomedy");
		genreMap.put("/m/014dsx","Travel");
		genreMap.put("/m/03gq4d1","Vampire movies");
		genreMap.put("/m/05b8xy","War effort");
		genreMap.put("/m/082gq","War film");
		genreMap.put("/m/021pxw","Werewolf fiction");
		genreMap.put("/m/0hfjk","Western");
		genreMap.put("/m/0gfc_2y","Western");
		genreMap.put("/m/0j5nm","Whodunit");
		genreMap.put("/m/05r6jc","Women in prison films");
		genreMap.put("/m/0hj3nyp","Workplace Comedy");
		genreMap.put("/m/03q4nz","World cinema");
		genreMap.put("/m/084r0","World History");
		genreMap.put("/m/08322","Wuxia");
		genreMap.put("/m/02pfsdf","Z movie");
		genreMap.put("/m/0jb4p32","Zombie Film");

		for(Entry<String, String> entry : genreMap.entrySet()) {
			genreFilterBox.addItem(entry.getValue(), entry.getKey());
		}
	}

	private void getCountriesDiagram() {
		countryMapDiagram.put("/m/0jdd","Afghanistan");
		countryMapDiagram.put("/m/0jdx","Albania");
		countryMapDiagram.put("/m/0h3y","Algeria");
		countryMapDiagram.put("/m/0jgd","Argentina");
		countryMapDiagram.put("/m/0jgx","Armenia");
		countryMapDiagram.put("/m/0j11","Aruba");
		countryMapDiagram.put("/m/0chghy","Australia");
		countryMapDiagram.put("/m/0h7x","Austria");
		countryMapDiagram.put("/m/0jhd","Azerbaijan");
		countryMapDiagram.put("/m/0160w","Bahamas");
		countryMapDiagram.put("/m/0161c","Bahrain");
		countryMapDiagram.put("/m/0162b","Bangladesh");
		countryMapDiagram.put("/m/0154j","Belgium");
		countryMapDiagram.put("/m/07bxhl","Bhutan");
		countryMapDiagram.put("/m/0165v","Bolivia");
		countryMapDiagram.put("/m/0166b","Bosnia and Herzegovina");
		countryMapDiagram.put("/m/015fr","Brazil");
		countryMapDiagram.put("/m/015qh","Bulgaria");
		countryMapDiagram.put("/m/01699","Burkina Faso");
		countryMapDiagram.put("/m/04xn_","Burma");
		countryMapDiagram.put("/m/01xbgx","Cambodia");
		countryMapDiagram.put("/m/01nln","Cameroon");
		countryMapDiagram.put("/m/0d060g","Canada");
		countryMapDiagram.put("/m/01p1v","Chile");
		countryMapDiagram.put("/m/0d05w3","China");
		countryMapDiagram.put("/m/01ls2","Colombia");
		countryMapDiagram.put("/m/01rxw","Congo");
		countryMapDiagram.put("/m/01p8s","Costa Rica");
		countryMapDiagram.put("/m/01rbb","Crime");
		countryMapDiagram.put("/m/01pj7","Croatia");
		countryMapDiagram.put("/m/0d04z6","Cuba");
		countryMapDiagram.put("/m/01ppq","Cyprus");
		countryMapDiagram.put("/m/01mjq","Czech Republic");
		countryMapDiagram.put("/m/01mk6","Czechoslovakia");
		countryMapDiagram.put("/m/088xp","Democratic Republic of the Congo");
		countryMapDiagram.put("/m/0k6nt","Denmark");
		countryMapDiagram.put("/m/02k54","Egypt");
		countryMapDiagram.put("/m/02jx1","England");
		countryMapDiagram.put("/m/02kmm","Estonia");
		countryMapDiagram.put("/m/019pcs","Ethiopia");
		countryMapDiagram.put("/m/018j0w","Federal Republic of Yugoslavia");
		countryMapDiagram.put("/m/02vzc","Finland");
		countryMapDiagram.put("/m/0f8l9c","France");
		countryMapDiagram.put("/m/0d0kn","Georgia");
		countryMapDiagram.put("/m/0247my","Georgian SSR");
		countryMapDiagram.put("/m/03f2w","German Democratic Republic");
		countryMapDiagram.put("/m/04306rv","German Language");
		countryMapDiagram.put("/m/0345h","Germany");
		countryMapDiagram.put("/m/035qy","Greece");
		countryMapDiagram.put("/m/03676","Guinea");
		countryMapDiagram.put("/m/036b_","Guinea-Bissau");
		countryMapDiagram.put("/m/03gyl","Haiti");
		countryMapDiagram.put("/m/03h64","Hong Kong");
		countryMapDiagram.put("/m/03gj2","Hungary");
		countryMapDiagram.put("/m/03rj0","Iceland");
		countryMapDiagram.put("/m/03rk0","India");
		countryMapDiagram.put("/m/03ryn","Indonesia");
		countryMapDiagram.put("/m/03shp","Iran");
		countryMapDiagram.put("/m/0d05q4","Iraq");
		countryMapDiagram.put("/m/0329by","Iraqi Kurdistan");
		countryMapDiagram.put("/m/03rt9","Ireland");
		countryMapDiagram.put("/m/03t1s","Isle of Man");
		countryMapDiagram.put("/m/03spz","Israel");
		countryMapDiagram.put("/m/03rjj","Italy");
		countryMapDiagram.put("/m/03_r3","Jamaica");
		countryMapDiagram.put("/m/03_3d","Japan");
		countryMapDiagram.put("/m/03__y","Jordan");
		countryMapDiagram.put("/m/019rg5","Kenya");
		countryMapDiagram.put("/m/014tss","Kingdom of Great Britain");
		countryMapDiagram.put("/m/02psqkz","Kingdom of Italy");
		countryMapDiagram.put("/m/048fz","Korea");
		countryMapDiagram.put("/m/047yc","Kuwait");
		countryMapDiagram.put("/m/04hqz","Lebanon");
		countryMapDiagram.put("/m/04gqr","Libya");
		countryMapDiagram.put("/m/04gzd","Lithuania");
		countryMapDiagram.put("/m/04g61","Luxembourg");
		countryMapDiagram.put("/m/04thp","Macau");
		countryMapDiagram.put("/m/0999q","Malayalam Language");
		countryMapDiagram.put("/m/09pmkv","Malaysia");
		countryMapDiagram.put("/m/04v09","Mali");
		countryMapDiagram.put("/m/04v3q","Malta");
		countryMapDiagram.put("/m/0h44w","Mandatory Palestine");
		countryMapDiagram.put("/m/0b90_r","Mexico");
		countryMapDiagram.put("/m/04w58","Monaco");
		countryMapDiagram.put("/m/04w8f","Mongolia");
		countryMapDiagram.put("/m/056vv","Montenegro");
		countryMapDiagram.put("/m/04wgh","Morocco");
		countryMapDiagram.put("/m/059z0","Nazi Germany");
		countryMapDiagram.put("/m/016zwt","Nepal");
		countryMapDiagram.put("/m/059j2","Netherlands");
		countryMapDiagram.put("/m/0ctw_b","New Zealand");
		countryMapDiagram.put("/m/05cgv","Nigeria");
		countryMapDiagram.put("/m/05bcl","Northern Ireland");
		countryMapDiagram.put("/m/05b4w","Norway");
		countryMapDiagram.put("/m/05sb1","Pakistan");
		countryMapDiagram.put("/m/0nyg5","Palestinian territories");
		countryMapDiagram.put("/m/0604m","Palestinian Territories");
		countryMapDiagram.put("/m/05qx1","Panama");
		countryMapDiagram.put("/m/016wzw","Peru");
		countryMapDiagram.put("/m/05v8c","Philippines");
		countryMapDiagram.put("/m/05qhw","Poland");
		countryMapDiagram.put("/m/05r4w","Portugal");
		countryMapDiagram.put("/m/05r7t","Puerto Rico");
		countryMapDiagram.put("/m/0697s","Qatar");
		countryMapDiagram.put("/m/0h66xzc","Republic of China");
		countryMapDiagram.put("/m/0bjv6","Republic of Macedonia");
		countryMapDiagram.put("/m/06c1y","Romania");
		countryMapDiagram.put("/m/06bnz","Russia");
		countryMapDiagram.put("/m/06q1r","Scotland");
		countryMapDiagram.put("/m/06srk","Senegal");
		countryMapDiagram.put("/m/077qn","Serbia");
		countryMapDiagram.put("/m/06swt","Serbia and Montenegro");
		countryMapDiagram.put("/m/06t2t","Singapore");
		countryMapDiagram.put("/m/07hj4m","Slovak Republic");
		countryMapDiagram.put("/m/06npd","Slovakia");
		countryMapDiagram.put("/m/06t8v","Slovenia");
		countryMapDiagram.put("/m/01rhrd","Socialist Federal Republic of Yugoslavia");
		countryMapDiagram.put("/m/0hzlz","South Africa");
		countryMapDiagram.put("/m/06qd3","South Korea");
		countryMapDiagram.put("/m/033fcj","Soviet occupation zone");
		countryMapDiagram.put("/m/05vz3zq","Soviet Union");
		countryMapDiagram.put("/m/06mkj","Spain");
		countryMapDiagram.put("/m/06m_5","Sri Lanka");
		countryMapDiagram.put("/m/0d0vqn","Sweden");
		countryMapDiagram.put("/m/06mzp","Switzerland");
		countryMapDiagram.put("/m/06f32","Taiwan");
		countryMapDiagram.put("/m/07f1x","Thailand");
		countryMapDiagram.put("/m/07fj_","Tunisia");
		countryMapDiagram.put("/m/01znc_","Turkey");
		countryMapDiagram.put("/m/01c4pv","Turkmenistan");
		countryMapDiagram.put("/m/07t21","Ukraine");
		countryMapDiagram.put("/m/0212ny","Ukrainian SSR");
		countryMapDiagram.put("/m/0jzptwn","Ukranian SSR");
		countryMapDiagram.put("/m/0j1z8","United Arab Emirates");
		countryMapDiagram.put("/m/07ssc","United Kingdom");
		countryMapDiagram.put("/m/09c7w0","United States of America");
		countryMapDiagram.put("/m/07twz","Uruguay");
		countryMapDiagram.put("/m/0247pq","Uzbek SSR");
		countryMapDiagram.put("/m/07t_x","Uzbekistan");
		countryMapDiagram.put("/m/07ylj","Venezuela");
		countryMapDiagram.put("/m/01crd5","Vietnam");
		countryMapDiagram.put("/m/0j5g9","Wales");
		countryMapDiagram.put("/m/084n_","Weimar Republic");
		countryMapDiagram.put("/m/082fr","West Germany");
		countryMapDiagram.put("/m/087vz","Yugoslavia");
		countryMapDiagram.put("/m/088vb","Zambia");
		countryMapDiagram.put("/m/088q4","Zimbabwe");

		for(Entry<String, String> entry : countryMapDiagram.entrySet()) {
			countryFilterBoxDiagram.addItem(entry.getValue(), entry.getKey());
		}
	}
	
	/*create and draw a PieChart */
	private PieChart drawChart(final String key, final String country) {
		
		final PieChart chart = new PieChart();
		
		// Get genre counts for selected country.
		FilmFilter filter = new FilmFilter();
		filter.setCountryFilter(key);
		filmService.countGroupBy(filter, "Genres", new AsyncCallback<List<FilmCountData>>() {
		
			@Override
			public void onFailure(Throwable caught) {
				showError(caught);
			}

			@Override
			public void onSuccess(List<FilmCountData> result) {
				// Create data table.
				DataTable dataTable = DataTable.create();
				dataTable.addColumn(ColumnType.STRING, "Genre");
				dataTable.addColumn(ColumnType.NUMBER, "Number of movies");
				
				// Fill data table.
				for (FilmCountData r : result) {
					dataTable.addRow(r.getValue(), r.getCount());
				}
				
				// Set options.
				PieChartOptions options = PieChartOptions.create();
				options.setWidth(1200);
				options.setHeight(500);
				options.setBackgroundColor("#fffaf7");
				options.setFontName("Tahoma");
				options.setIs3D(true);
				options.setPieResidueSliceColor("BLACK");
				options.setPieResidueSliceLabel("Others");
				options.setSliceVisibilityThreshold(0.01);
				options.setTitle("Genre distribution over all years for "+country);

				// Draw the chart.
				chart.draw(dataTable, options);
			}
		});

		return chart;
	}
}
